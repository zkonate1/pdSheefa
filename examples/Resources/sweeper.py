import sys
import spin
import libSPINPyWrap
import math
import random

__spin_behavior_class__ = "Script"

class Script( spin.ScriptBase ):


    def __init__(self, id, scl):
        
        self._nodeID = id

        # store last yaw
        ret = libSPINPyWrap.callback(self._nodeID, "getOrientation", [], 0)
        self.lastYaws,self.lastYaw = divmod(ret.getVector()[2], 360)

        # store a list of target node names:
        self.targets = ["target1", "target2", "target3", "target4","target5",
                        "target6", "target7", "target8", "target9","target10"]
        
        print "Loaded sweeper script"

    def run(self, eventMethod, eventArgs): 
        
        #print "node", self._nodeID, "got event: ", eventMethod, eventArgs
       
        # we first apply the event (pass it to the server for processing
        # so that the orientation is applied. The 3rd arguments describes
        # how events are cascaded. 0 means that the event is processed,
        # but this run() method will not be re-invoked
        self.applyEvent(eventMethod, eventArgs, 0)

        # once the orientation has been applied, we get the
        # latest values
        ret = libSPINPyWrap.callback(self._nodeID, "getOrientation", [], 0)
        yaws,yaw = divmod(ret.getVector()[2], 360)
                
        if (yaws!=self.lastYaws):
            # we've crossed 0 deg; for this case, we subtract 360
            # from the higher yaw, resulting in a negative value,
            # thus, the difference stradles zero
            if (yaw > self.lastYaw):
                yaw -= 360
            else:
                self.lastYaw-=360

        # also get our current translation
        ret = libSPINPyWrap.callback(self._nodeID, "getTranslation", [], 0)
        x,y,z = ret.getVector()

        for t in self.targets:
            try:
                ret = libSPINPyWrap.callback(t, "getTranslation", [], 0)
                targetVec = ret.getVector()
                
                if len(targetVec) != 3:
                    # the node t doesn't exist
                    continue

                angle = -90 + math.degrees(math.atan2(targetVec[1]-y,targetVec[0]-x))
                angle = angle % 360

                #print "yaw=", yaw, ", lastYaw=", self.lastYaw, ", angle to ", t, "= ", angle

                if ( ((angle<yaw) and (angle>self.lastYaw))
                  or ((angle>yaw) and (angle<self.lastYaw)) ):
               
                    # we crossed the target
                    print "Sweeper passed ", t
                    ret = libSPINPyWrap.callback(t, "setScale", [0.2, 0.2, 0.2], 0)
                
                else:
                    ret = libSPINPyWrap.callback(t, "setScale", [0.1, 0.1, 0.1], 0)

            except:
                print "ignoring ", t, ". Error: ", sys.exc_info()[0]


        self.lastYaw = yaw % 360
        self.lastYaws = yaws 

print "sweeper script successfully loaded"
