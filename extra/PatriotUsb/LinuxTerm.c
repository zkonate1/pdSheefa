/************************************************************************

		POLHEMUS PROPRIETARY

		Polhemus Inc.
		P.O. Box 560
		Colchester, Vermont 05446
		(802) 655-3159



        		
	    Copyright © 2004 by Polhemus
		All Rights Reserved.


*************************************************************************/

// SampTerm.c		Sample code to illustrate how to interface to a Liberty/Patriot over Linux via USB


#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <termios.h>


void GetPno();
void GetBinPno();
int GetVerInfo(char*);

enum{PASS,FAIL};

int rdPort,wrPort;


int main(){

	int exit=0;			
	char choice[10];
	char buf[1000];
	char trakType[30];
	int br;
	struct termios initialAtt,newAtt;

	rdPort=open("/dev/ttyUSB0",O_RDONLY|O_NDELAY);
	wrPort=open("/dev/ttyUSB0",O_WRONLY);
	if ((rdPort==-1) || (wrPort==-1)){
		printf("Error connecting to tracker\n");
		return -1;
	}

	// set up terminal for raw data
	tcgetattr(rdPort,&initialAtt);		// save this to restore later
	newAtt=initialAtt;
	cfmakeraw(&newAtt);
	if (tcsetattr(rdPort,TCSANOW,&newAtt)){
		printf("Error setting terminal attributes\n");
		close(rdPort);
		close(wrPort);
		return -2;
	}


	// determine tracker type
	GetVerInfo(buf);
	if (strstr(buf,"Patriot"))
		strcpy(trakType,"Patriot");
	else if(strstr(buf,"Liberty"))
		strcpy(trakType,"Liberty");
	else
		strcpy(trakType,"Unknown tracker");

	printf("\nConnected to %s\n",trakType);

	memset(buf,0,100);


	while (!exit){

		printf("\nEnter P for Position and Orientation data\n");
		printf("Enter B to Obtain Position and Orientation as Binary Data\n");
		printf("Enter V to Obtain Version information\n");
		printf("Enter X to exit\n");
		fgets(choice,10,stdin);
		tolower(choice[0]);

		switch (choice[0]) {
		case 'x':
			exit=1;
			break;

		case 'p':
			GetPno();
			break;

		case 'b':
			GetBinPno();
			break;

		case 'v':
			GetVerInfo(NULL);
			break;

		default:
			printf("Not a proper choice\n");

		}  // end switch

		printf("\n");

	}

	tcsetattr(rdPort,TCSANOW,&initialAtt);		// restore the original attributes
	close(rdPort);
	close(wrPort);
	return 0;


}



void GetPno(){

	int br,count;
	char buf[1000];		// may need to be larger if getting alot of data



	read(rdPort,buf,1000);
	memset(buf,0,1000);
	
	count=0;
	write(wrPort,"p",1);			// request data
	do {
		br=read(rdPort,buf,1000);
		usleep(1000);			// wait 1ms
	} while ((br<=0) && (count++<100));		// try to read 10 times


	buf[br]='\0';	//terminate
	printf("\n%d bytes Read\n",br);
	

	printf(buf);
}

void GetBinPno(){

	char buf[1000];
	char hex[200];
	char tmp[10];
	int i=0;
	int br,count;

	memset(buf,0,1000);

	write(wrPort,"f1\r",3);			// put tracker into binary mode

	write(wrPort,"p",1);			// request data\

	count=0;
	do {
		br=read(rdPort,buf,1000);
		usleep(5000);			// 5 ms delay
	} while ((br<=0) && (count++<500));	

	if (br<=0){
		write(wrPort,"f0\r",3);
		printf("Error reading tracker data\n");
		return;
	}

	buf[br]='\0';


	write(wrPort,"f0\r",3);			// return tracker to ASCII


	if (strncmp(buf,"LY",2) && strncmp(buf,"PA",2)){		// check for proper format

		printf("Corrupted data received\n");
		printf(buf);
		return;
	}

	float* pData=(float*)(buf+8);			// header is first 8 bytes


	memset(hex,0,200);
	printf("\nBytesRead: %d\n",br);
	printf("Binary Data Received:\n");		// just to show what was actually received -- station 1 only
	for (i=0;i<br;i++){
		sprintf(tmp,"%0.2X ",buf[i]&0xFF);
		strcat(hex,tmp);
	}
	strcat(hex,"\n");
	printf(hex);
	printf("\nTranslated:\n");


	// print data
	printf("x= %.4f, y= %.4f, z= %.4f, az= %.4f, el= %.4f, roll= %.4f\n",	\
		pData[0],pData[1],pData[2],pData[3],pData[4],pData[5]);



}

// return version info in info
// if info is NULL, just write the info to the screen
// return 0 for success, -1 for failure
int GetVerInfo(char* info){
	
	char cmd[10];
	char buf[400];
	int br;
	int rv=0;
	memset(buf,0,400);
	sprintf(cmd,"%c\r",22);
	write(wrPort,cmd,strlen(cmd));
	usleep(100000);			// wait 100 ms
	br=read(rdPort,buf,400);
	if (br>0){
		if (info)
			strcpy(info,buf);
		else
			printf(buf);
	}

	else{
		printf("Error obtaining version information\n");
		rv=-1;
	}

	return rv;


}


