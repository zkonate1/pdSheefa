#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h> 
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <termios.h> 
#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h> 
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>

#include <pthread.h>

#include <math.h>

#include <string>
#include <vector>
#include <sstream>

#include <iomanip>

#include "GPSutil.h"


using namespace std;



#define MAX_BUFFER_SIZE 1024
#define MAX_NMEA_SIZE 85 // max length for NMEA strings is actually 82


static t_class *serialGPS_class;

static pthread_mutex_t pthreadLock = PTHREAD_MUTEX_INITIALIZER;

typedef struct _gpstrack
{
    bool active;
    t_symbol *type;
    string data;
    OGRLineString *ogrTrack;
} gpstrack;


typedef struct _serialGPS {

	t_object x_obj; //obj needed by pd   	

	// serial connection stuff:
	t_symbol *addr;
	int baudrate;
	int sock;

	// Keep the NMEA string here (but remember to enable a thread mutex while
	// working with it, because the poll() thread method will be constantly
	// writing over it:
	char msg[MAX_BUFFER_SIZE];
	
	// NMEA parser:
	char nmea[MAX_NMEA_SIZE];
	nmeaPARSER NMEAparser;
	nmeaINFO NMEAinfo;
	nmeaPOS prevPosition;
	double thresholdDistance;
	
	bool updateFlag;
	
	// GPX writer:
	OGRDataSource *ogrDS;
    OGRLayer *waypointLayer;
    OGRLayer *trackLayer;

    
    std::vector<gpstrack> tracks;
    
    OGRSpatialReference *spatialRef;
    
    
	bool debugFlag;
    
	// for auto mode:
	int loggingMode;
	int loggingIndex;
	

	// the start time of this log file:
    struct timeval recordStartTime;

	t_clock *x_clock; // Pd clock used for periodic polling

	int status;
	double lastUpdateTime;
	
	// pthread stuff
	pthread_t pthreadID; // id of child thread
	pthread_attr_t pthreadAttr;

	t_outlet *dataOutlet;
	t_outlet *statusOutlet;

	t_atom outData[3];

} t_serialGPS;


// declare some functions in advance:
void serialGPS_updateStatus(t_serialGPS *x);
void serialGPS_parseNMEA(t_serialGPS *x);

// *****************************************************************************
// *****************************************************************************
// serial READING methods (from GPS receiver):
// *****************************************************************************
// *****************************************************************************


// baudrate settings are defined in <asm/termbits.h>, included by <termios.h>
int makeSerialPortSocket(char port[],int baud )
{
	int fd;
	struct termios newtio;

	fd = open(port,O_RDWR); // ope nup the port on read/ write mode
	if (fd == -1)
	return(-1); // Opps. We just has an error

	/* Save the current serial port settings */
	tcgetattr(fd, &newtio);

	/* Set the input/output baud rates for this device */
	cfsetispeed(&newtio, baud); //115200

	/* CLOCAL:      Local connection (no modem control) */
	/* CREAD:       Enable the receiver */
	newtio.c_cflag |= (CLOCAL | CREAD);

	/* PARENB:      Use NO parity */
	/* CSTOPB:      Use 1 stop bit */
	/* CSIZE:       Next two constants: */
	/* CS8:         Use 8 data bits */
	newtio.c_cflag &= ~PARENB;
	newtio.c_cflag &= ~CSTOPB;
	newtio.c_cflag &= ~CSIZE;
	newtio.c_cflag |= CS8;

	/* Disable hardware flow control */
	// BAD:  newtio.c_cflag &= ~(CRTSCTS);

	/* ICANON:      Disable Canonical mode */
	/* ECHO:        Disable echoing of input characters */
	/* ECHOE:       Echo erase characters as BS-SP-BS */
	/* ISIG:        Disable status signals */
	// BAD: newtio.c_lflag = (ECHOK);

	/* IGNPAR:      Ignore bytes with parity errors */
	/* ICRNL:       Map CR to NL (otherwise a CR input on the other computer will not terminate input) */
	// BAD:  newtio.c_iflag |= (IGNPAR | ICRNL);
	newtio.c_iflag |= (IGNPAR | IGNBRK);

	/* NO FLAGS AT ALL FOR OUTPUT CONTROL  -- Sean */
	newtio.c_oflag = 0;

	/* IXON:        Disable software flow control (incoming) */
	/* IXOFF:       Disable software flow control (outgoing) */
	/* IXANY:       Disable software flow control (any character can start flow control */
	newtio.c_iflag &= ~(IXON | IXOFF | IXANY);

	/* NO FLAGS AT ALL FOR LFLAGS  -- Sean*/
	newtio.c_lflag = 0;

	/*** The following settings are deprecated and we are no longer using them (~Peter) ****/
	// cam_data.newtio.c_lflag &= ~(ICANON && ECHO && ECHOE && ISIG); 
	// cam_data.newtio.c_lflag = (ECHO);
	// cam_data.newtio.c_iflag = (IXON | IXOFF);
	/* Raw output */
	// cam_data.newtio.c_oflag &= ~OPOST;

	/* Clean the modem line and activate new port settings */
	tcflush(fd, TCIOFLUSH);
	tcsetattr(fd, TCSANOW, &newtio);

	return(fd);
}




// *****************************************************************************
void serialGPS_disconnect(t_serialGPS *x)
{
	
	if (x->sock > 0)
	{
		clock_unset(x->x_clock);
		nmea_zero_INFO(&x->NMEAinfo);
		
		pthread_mutex_lock(&pthreadLock);
		close(x->sock);
		x->sock = -1;
		pthread_mutex_unlock(&pthreadLock);

		serialGPS_updateStatus(x);
	}
	
}


// *****************************************************************************
void *serialGPS_poll(void *arg)
{
	fd_set rset;
	int ret, i;
	char buf[MAX_BUFFER_SIZE];
	char nmeaTMP[MAX_NMEA_SIZE];

	int index = 0;
	
	t_serialGPS *x = (t_serialGPS *)arg;
	
	// create a Pd clock, that will call the parseNMEA method
	// (and call it once to start):
	x->x_clock = clock_new(x, (t_method)serialGPS_parseNMEA);
	serialGPS_parseNMEA(x);
	
	while (1)
	{
		if (x->sock <= 0)
		{
			pthread_exit(NULL);
			return 0;		
		}
		
		FD_ZERO(&rset);
		FD_SET(x->sock,&rset);
	
		ret = select(x->sock+1, &rset, NULL, NULL, NULL);
		if (ret < 0)
		{
			serialGPS_disconnect(x);
			continue;
		}
		
		if (FD_ISSET(x->sock,&rset))
		{
			// There's stuff to read!

			ret = read(x->sock, buf, MAX_BUFFER_SIZE);
			if (ret<0)
			{
				serialGPS_disconnect(x);
				continue;
			}
			else if (ret>0)
			{
				for (i=0; i<ret; i++)
				{
					// If the next char is a $, then we've reached the end of an
					// NMEA message.
					if (buf[i]=='$')
					{
						if (strlen(nmeaTMP)) {
							
							pthread_mutex_unlock(&pthreadLock);
							memcpy(x->nmea, nmeaTMP, MAX_NMEA_SIZE);
							x->updateFlag = true;
							pthread_mutex_unlock(&pthreadLock);
							
						}
						
						memset (nmeaTMP,'\0',MAX_NMEA_SIZE);
						index = 0;
					}
					
					// otherwise, no complete message has been found, so we add
					// to the x->nmea buffer and continue
					if (buf[i] != '\n')
					{
						memcpy(nmeaTMP + index, buf+i, 1);
						index++;
					}
				}
			}
		}
	}
	
	return 0;
}


// *****************************************************************************
void serialGPS_connect(t_serialGPS *x, t_symbol *targetAddress, t_floatarg targetBaud)
{
	x->addr = targetAddress;
	x->baudrate = targetBaud;

	switch ((int)targetBaud)
	{
		case 115200:
			x->baudrate = B115200;
			break;
		case 38400:
			x->baudrate = B38400;
			break;
		case 19200:
			x->baudrate = B19200;
			break;
		case 9600:
			x->baudrate = B9600;
			break;
		default:
			post("ERROR!: Unknown baud rate: %d\n", (int) targetBaud);
			return;
			break;
	}

	x->sock = makeSerialPortSocket(x->addr->s_name, x->baudrate);

	if (x->sock <= 0)
	{
		post("[serialGPS] ERROR: couldn't open serial port!\n");
		close(x->sock);
		x->sock = -1;
		return;
	}
	else
	{	
		post("Listening to GPS data (NMEA format) on: %s", x->addr->s_name);
		x->lastUpdateTime = clock_getsystime();
	
		// create child thread which will poll for data:
		if (pthread_attr_init(&x->pthreadAttr) < 0)
			post("serialGPS: warning: could not prepare child thread\n");
		if(pthread_attr_setdetachstate(&x->pthreadAttr, PTHREAD_CREATE_DETACHED) < 0)
			post("serialGPS: warning: could not prepare child thread\n");
		if (pthread_create(&x->pthreadID, &x->pthreadAttr, serialGPS_poll, x) < 0)
			post("serialGPS: could not create new thread\n");
	
	}
}


// *****************************************************************************
void serialGPS_updateStatus(t_serialGPS *x)
{
	int newStatus = 0;

	if (x->sock > 0)
	{
		double timenow = clock_getsystime();
		if ((timenow - x->lastUpdateTime) > 200000000) // ~10 sec
		{
			// no data, so not connected anymore??
			serialGPS_disconnect(x);
			return;
			
		} else {
			if (x->NMEAinfo.sig==1) newStatus=2; // fix
			else if (x->NMEAinfo.sig>1) newStatus=3; // differential
			else newStatus = 1; // just connected to receiver
		}
	}
	
	if (newStatus != x->status)
	{
		x->status = newStatus;
		outlet_float(x->statusOutlet, x->status);
	}
		
}






// *****************************************************************************
// *****************************************************************************
// serial WRITING methods (to send events TO the GPS receiver):
// *****************************************************************************
// *****************************************************************************


int write_serial(t_serialGPS *x, unsigned char serial_byte)
{
	int result = write(x->sock,(char *) &serial_byte,1);
	if (result != 1)
	{
		post ("[serialGPS] write returned %d, errno is %d", result, errno);
	}
	return result;
}

void serialGPS_sendFloat(t_serialGPS *x, t_floatarg f)
{
	unsigned char serial_byte = ((int) f) & 0xFF; /* brutal conv */

	if (write_serial(x,serial_byte) != 1)
	{
		post("[serialGPS] write error, maybe TX-OVERRUNS on serial line");
	}
}

void serialGPS_setDGPS(t_serialGPS *x, t_floatarg flag)
{
	char cmd[512];

	if (x->sock<0) return;

	else
	{

		//DGPS enabling ubx message:
		// - subsystem enabled
		// - Apply SBAS correction data
		// - nb of search channel = 3
		// - PRN codes = WAAS
		//B5 62 06 16 08 00 01 02 03 00 04 C0 04 00 F2 0A

		unsigned char buffertowrite[] = {
			0xB5, 0x62, 0x06, 0x16, 0x08, 0x00,
			0x01, 0x02, 0x03, 0x00, 0x04, 0xC0,
			0x04, 0x00, 0xF2, 0x0A};

		int result= write(x->sock,(char *) buffertowrite, 16);
		write(x->sock, "\r", 1);
		if(result == 16) post("serialGPS: enabled DGPS mode");
		else post("[serialGPS] DGPS mode sending ubx-cfg-sbas message failed");
	}

}






// *****************************************************************************
// *****************************************************************************
//  Logging to file (GPX, using the GDAL/OGR library):
// *****************************************************************************
// *****************************************************************************



void serialGPS_doWaypoint(t_serialGPS *x, t_symbol *type, const char *data)
{	
	
	/* We create a local OGRFeature, set attributes and attach geometry
	 * before trying to write it to the layer, but it is imperative that the
	 * feature be instantiated from the OGRFeatureDefn associated with the
	 * layer it will be written to.
	 */
	
	OGRFeature *poFeature = OGRFeature::CreateFeature( x->waypointLayer->GetLayerDefn() );
	
	
	// NOTE: lat/long are stored as NMEA degrees. We need to convert to
	// fractional degrees before using these.
	
    OGRPoint pt;
    pt.setX( nmea_ndeg2degree(x->NMEAinfo.lon) );
	pt.setY( nmea_ndeg2degree(x->NMEAinfo.lat) );
	pt.setZ( x->NMEAinfo.elv );
	
	/* OGRFeature::SetGeometryDirectly() differs from SetGeometry() in that
	 * the direct method gives ownership of the geometry to the feature.
	 * This is generally more efficient as it avoids an extra deep object
	 * copy of the geometry. However, if it does not take ownership of our
	 * feature, we must clean it up with OGRFeature::DestroyFeature()
	 */
	
	poFeature->SetGeometryDirectly( &pt ); 
	
	nmeaTIME now;
	nmea_time_now(&now);

	poFeature->SetField(0,x->NMEAinfo.elv);
	poFeature->SetField(1,1900+now.year, now.mon, now.day, now.hour, now.min, now.sec, 0); // 1 means local time

	// the type of feature goes in the 'name' field
	poFeature->SetField(4, type->s_name);

	// put the elapsed time since start of recording in the comment field:
	struct timeval elapsedTime;
	gettimeofday(&elapsedTime, NULL);
	struct timeval dt;
	timeval_subtract(&dt, &x->recordStartTime, &elapsedTime);
	
	char *buf = new char[128];
	sprintf (buf, "%d.%d", (int)(-dt.tv_sec), (int)(dt.tv_usec));
	poFeature->SetField(5,buf);
	
	
	poFeature->SetField(6,data);

	switch(x->NMEAinfo.sig)
	{
		case 0:
			poFeature->SetField(15,"Invalid");
			break;
		case 1:
			poFeature->SetField(15,"Fix");
			break;
		case 2:
			poFeature->SetField(15,"Differential");
			break;
		case 3:
			poFeature->SetField(15,"Sensitive");
			break;		
	}

	switch(x->NMEAinfo.fix)
	{
		case 0:
			poFeature->SetField(16,"Unknown");
			break;
		case 1:
			poFeature->SetField(16,"Fix not available");
			break;
		case 2:
			poFeature->SetField(16,"2D");
			break;
		case 3:
			poFeature->SetField(16,"3D");
			break;		
	}
	poFeature->SetField(17,x->NMEAinfo.satinfo.inuse);	
	poFeature->SetField(18,x->NMEAinfo.HDOP);
	poFeature->SetField(19,x->NMEAinfo.VDOP);
	poFeature->SetField(20,x->NMEAinfo.PDOP);

	
	
	if( x->waypointLayer->CreateFeature( poFeature ) != OGRERR_NONE )
	{
		post( "[serialGPS] Failed to record waypoint.\n" );
	}

	//OGRFeature::DestroyFeature( poFeature );

}



// *****************************************************************************
void serialGPS_makeWaypoint(t_serialGPS *x, t_symbol *s, int argc, t_atom *argv)
{
	t_symbol *type;
	ostringstream data("");

	if (!x->ogrDS) {
		post("[serialGPS] ERROR: No log file defined. Use the createLog method to provide a filename.");
		return;
	}
	
	// check args (there needs to be at least one symbolic arg for type):
	if ((argc) && (argv[0].a_type == A_SYMBOL))
	{
		type = argv[0].a_w.w_symbol;
	} else
	{
		post("makeWaypoint requires a type symbol (which can be proceeded with any list of data)");
		return;
	}
	
	for (int i=1; i<argc; i++)
	{
		if (i>1) data << " ";
		if (argv[0].a_type == A_SYMBOL) data << argv[i].a_w.w_symbol->s_name;
		else if (argv[0].a_type == A_FLOAT) data << argv[i].a_w.w_float;
	}

	serialGPS_doWaypoint(x,type,data.str().c_str());
}


// *****************************************************************************
void serialGPS_stopTrack(t_serialGPS *x)
{
	// stop the last track (only one can be active at a time)
	if (x->tracks.size())
		x->tracks[x->tracks.size()-1].active = false;
	
}
// *****************************************************************************
void serialGPS_startTrack(t_serialGPS *x, t_symbol *s, int argc, t_atom *argv)
{
	t_symbol *type;
	ostringstream data("");

	if (!x->ogrDS) {
		post("[serialGPS] ERROR: No log file defined. Use the createLog method to provide a filename.");
		return;
	}
	
	// in case there is an active track, then stop that one first (only one can
	// be active at a time)
	serialGPS_stopTrack(x);
	
	// check args (there needs to be at least one symbolic arg for type):
	if ((argc) && (argv[0].a_type == A_SYMBOL))
	{
		type = argv[0].a_w.w_symbol;
	} else
	{
		post("startTrack requires a type (and then an optional list of data)");
		return;
	}
	
	// make data string:
	for (int i=1; i<argc; i++)
	{
		if (i>1) data << " ";
		if (argv[0].a_type == A_SYMBOL) data << (char*) argv[i].a_w.w_symbol->s_name;
		else if (argv[0].a_type == A_FLOAT) data << (float) argv[i].a_w.w_float;
	}


    // Create a new gpstrack and push it onto the list. Note that we can't write
	// tracks to file right away because GPX requires that all waypoints be
	// written before tracks. Thus, we store them until the writeLog method is
	// called.
	
    gpstrack newTrack;
    
    newTrack.type = type;
    newTrack.data = data.str();
    newTrack.ogrTrack = new OGRLineString();
    newTrack.active = true;
    x->tracks.push_back(newTrack);

    
    // record the current position in the track
    newTrack.ogrTrack->addPoint(
    		nmea_ndeg2degree(x->NMEAinfo.lon),
    		nmea_ndeg2degree(x->NMEAinfo.lat),
    		x->NMEAinfo.elv);
}






// *****************************************************************************
void serialGPS_createLog(t_serialGPS *x, t_symbol *filename)
{
	
	// info about the GPX driver: http://www.gdal.org/ogr/drv_gpx.html
	const char *pszDriverName = "GPX";
	
	//const char *pszDriverName = "ESRI Shapefile";

 
	
	// First make sure the driver is available to store into our desired format:
	OGRSFDriver *poDriver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName( pszDriverName );
    if( poDriver == NULL )
    {
        post( "[serialGPS] Error: %s driver not available.", pszDriverName );
        return;
    }

    // Next, create the datasource:

    // NOTE: We could provide GPX_USE_EXTENSIONS as creation option to allow use
    // of the <extensions> element.
    //char *dsopts[] = { "GPX_USE_EXTENSIONS=YES", NULL };

    x->ogrDS = poDriver->CreateDataSource( filename->s_name, NULL );
    if( x->ogrDS == NULL )
    {
        post( "[serialGPS] Error: Creation of output file failed." );
        return;
    }

    // Define a spatial coordinate system reference:
    x->spatialRef = new OGRSpatialReference();
    

    
    // set the underlying geographic coordinate system to WGS84 ... note order
    // is important: set the projection first.
    if (x->spatialRef->SetWellKnownGeogCS("WGS84") != OGRERR_NONE)
    {
    	post( "[serialGPS] Error: Could not set up spatial reference." );    	
    }

    
    
     
    // Create the waypointLayer layer (supporting geometry wkbPoint):
    x->waypointLayer = x->ogrDS->CreateLayer( "SerialGPS_waypointLayer", x->spatialRef, wkbPoint25D, NULL);
    if( x->waypointLayer == NULL )
    {
        post( "[serialGPS] Error: Could not create waypointLayer layer." );
        return;
    }
    

    // Create the trackLayer layer (supporting geometry wkbLineString):
    char *layerOpts[] = { "FORCE_GPX_TRACK=YES", NULL };
    x->trackLayer = x->ogrDS->CreateLayer( "SerialGPS_trackLayer", x->spatialRef, wkbLineString, layerOpts);
    if( x->trackLayer == NULL )
    {
        post( "[serialGPS] Error: Could not create trackLayer layer." );
        return;
    }   

    
    


    if (x->debugFlag)
    {
    	for (int i=0; i<x->ogrDS->GetLayerCount(); i++)
    	{
		    OGRFeatureDefn *schema = x->ogrDS->GetLayer(i)->GetLayerDefn();
		    post("OGR recording using %s driver, on layer '%s'. Possible fields are:", pszDriverName, schema->GetName());
		    for (int i=0; i<schema->GetFieldCount(); i++)
		    {
		    	post("  field[%d]: '%s', type=%d, width=%d, precision=%d",
		    			i,
		    			schema->GetFieldDefn(i)->GetNameRef(),
		    			schema->GetFieldDefn(i)->GetType(),
		    			schema->GetFieldDefn(i)->GetWidth(),
		    			schema->GetFieldDefn(i)->GetPrecision());	 
		    }
    	}
    }
    
    gettimeofday (&x->recordStartTime, NULL);
    
    post("Started log %s", filename->s_name);

}

// *****************************************************************************
void serialGPS_writeLog(t_serialGPS *x)
{
	// Tracks have to be saved after all waypoints, so we have stored them in
	// x->tracks, but they now need to be made into OGRFeatures
	
	// First, if there is an active tracks, make sure it is stopped:
	serialGPS_stopTrack(x);
	
	vector<gpstrack>::iterator tr = x->tracks.begin();
	while (tr != x->tracks.end())
	{
	
		OGRFeature *poFeature = OGRFeature::CreateFeature( x->trackLayer->GetLayerDefn() );
		
		poFeature->SetGeometryDirectly( (*tr).ogrTrack ); 
	
		poFeature->SetField(0, (*tr).type->s_name); // name
		poFeature->SetField(2, (*tr).data.c_str()); // desc
	    
		if( x->trackLayer->CreateFeature( poFeature ) != OGRERR_NONE )
		{
			post( "[serialGPS] Failed to record track.\n" );
		}

		x->tracks.erase(tr); // iterator automatically advances after erase()
		
	}

	
	
	//x->ogrDS->SyncToDisk(); // do we need to do this?
	OGRDataSource::DestroyDataSource( x->ogrDS );
	x->ogrDS = NULL;
	
	post("Wrote log file.");
}



// *****************************************************************************
// *****************************************************************************
// Main Pd methods:
// *****************************************************************************
// *****************************************************************************


void serialGPS_info(t_serialGPS *x)
{

	post("\nDEBUG INFO:");	
	post("===========");
	
	
	nmeaTIME now;
	nmea_time_now(&now);
	
	post("  elv=%.5f", x->NMEAinfo.elv);
	post("  speed=%.5f", x->NMEAinfo.speed);
	post("  direction=%.5f", x->NMEAinfo.direction);
	post("  time(gps): %s", NMEATimeAsString(&x->NMEAinfo.utc) );
	post("  time(now): %s", NMEATimeAsString(&now) );
	
	post("  Sig: %d, Fix: %d, Elev:%.2f, Speed=%.2f, Dir=%.2f",
			x->NMEAinfo.sig, x->NMEAinfo.fix,
			x->NMEAinfo.elv, x->NMEAinfo.speed, x->NMEAinfo.direction);

	post("  NMEA lat/Lon: %.16f %.16f", x->NMEAinfo.lat, x->NMEAinfo.lon);
    // Convert NDEG (NMEA degree) to fractional degree
    post("  in degrees:   %.16f %.16f", nmea_ndeg2degree(x->NMEAinfo.lat), nmea_ndeg2degree(x->NMEAinfo.lon));

    // See precision loss in Pd:
	SETFLOAT(x->outData+0, nmea_ndeg2degree(x->NMEAinfo.lat));
	SETFLOAT(x->outData+1, nmea_ndeg2degree(x->NMEAinfo.lon));
	post("  as a Pd atom: %.16f %.16f", atom_getfloat(x->outData), atom_getfloat(x->outData+1));

}


void serialGPS_bang(t_serialGPS *x)
{
		
	// output the Lat/Long:

    SETFLOAT(x->outData+0, nmea_ndeg2degree(x->NMEAinfo.lat));
    SETFLOAT(x->outData+1, nmea_ndeg2degree(x->NMEAinfo.lon));
    SETFLOAT(x->outData+2, x->NMEAinfo.elv);
    outlet_anything(x->dataOutlet, gensym("LatLon"), 3, x->outData);

	
	// project to UTM:
	
	double UTM_X, UTM_Y, UTM_Z;
	UTM_X = nmea_ndeg2radian(x->NMEAinfo.lon);
	UTM_Y = nmea_ndeg2radian(x->NMEAinfo.lat);
	UTM_Z = x->NMEAinfo.elv;
	
	if (ConvertToUTM( &UTM_X, &UTM_Y, &UTM_Z ))
	{
		SETFLOAT(x->outData+0, UTM_X);
		SETFLOAT(x->outData+1, UTM_Y);
		SETFLOAT(x->outData+2, UTM_Z);
		outlet_anything(x->dataOutlet, gensym("UTM"), 3, x->outData);
	} else {
		post("[serialGPS] Error: Could not convert to UTM.");
	}

}


void serialGPS_setDebug(t_serialGPS *x, t_floatarg f)
{
	x->debugFlag = (int)f;
}


void serialGPS_setThreshold(t_serialGPS *x, t_floatarg f)
{
	x->thresholdDistance = f;
}

void serialGPS_setLoggingMode(t_serialGPS *x, t_floatarg f)
{
	x->loggingMode = (int)f;
}

void serialGPS_parseNMEA(t_serialGPS *x)
{	
	
	/* Data from the GPS receiver is stored in x->NMEAinfo during the poll().
	 *  - Info about NMEA: http://www.gpsinformation.org/dale/nmea.htm
	 *  - NMEAparser is from: http://nmea.sourceforge.net
	 */
	
	bool positionUpdate = false;
	
	if (x->updateFlag)
	{
		x->lastUpdateTime = clock_getsystime();
	
		// make sure to use mutex when reading x->nmea, because it can be
		// changing in the read thread
		pthread_mutex_lock(&pthreadLock);
		
		if (x->debugFlag) post("NMEA: %s", x->nmea);
		
		#ifdef __Linux					
			x->nmea[strlen(x->nmea)] = '\r';
		#endif
			x->nmea[strlen(x->nmea)] = '\n';
			
		nmea_parse(&x->NMEAparser, x->nmea, (int)strlen(x->nmea), &x->NMEAinfo);
		
		// A position update occurs only with GPGGA messages
		if (strncmp(x->nmea,"$GPGGA",6) == 0) positionUpdate = true;
		
		// let go of the mutex
		pthread_mutex_unlock(&pthreadLock);
		
		
		

		if (positionUpdate)
		{
			// We compare this position to the previous one, and
			// only act if there is a threshold change:
									
			nmeaPOS currentPos;
			nmea_info2pos(&x->NMEAinfo, &currentPos);
			double dp = nmea_distance(&x->prevPosition, &currentPos);
									
			if ((x->thresholdDistance==0) || (dp > x->thresholdDistance))
			{
				// a new position has been discovered, so let's do some stuff:
				
				if (x->debugFlag) serialGPS_info(x);
				
				// if a track is active, then add this position:
				if (x->tracks.size() && (x->tracks[x->tracks.size()-1].active))
				{
					x->tracks[x->tracks.size()-1].ogrTrack->addPoint(
							nmea_ndeg2degree(x->NMEAinfo.lon),
							nmea_ndeg2degree(x->NMEAinfo.lat),
							x->NMEAinfo.elv);
				}
				
				// if loggingMode is in auto, then add a waypoint for every new
				// location:
				if ((x->loggingMode==1) && (x->ogrDS != NULL)) {
					char *buf = new char[16];
					sprintf (buf, "%08d", x->loggingIndex++);
					serialGPS_doWaypoint(x, gensym("autolog"), buf);
				}
				
				// output realtime data from the outlets:
				serialGPS_bang(x);
				
				x->prevPosition = currentPos;					
			}
		}
		

	
		x->updateFlag = false;
	}
	
	// update the status:
	serialGPS_updateStatus(x);
	
	clock_delay(x->x_clock, 50);
}







// *****************************************************************************


void *serialGPS_new(t_symbol *addr)
{
	t_serialGPS *x = (t_serialGPS *)pd_new(serialGPS_class);
	
	nmea_parser_init(&x->NMEAparser);
	nmea_zero_INFO(&x->NMEAinfo);
	x->prevPosition.lat = 0;
	x->prevPosition.lon = 0;
	x->thresholdDistance = 0.1; // metres
	
	x->sock = -1;
	x->msg[0] = '\0';
	x->debugFlag = 0;

	x->loggingMode = 1; // 0=manual, 1=log_all_as_waypoints
	x->loggingIndex = 0;
	
	x->tracks.clear();
	
	x->status = 0;
	
	
	x->dataOutlet = outlet_new(&x->x_obj, &s_symbol);
	x->statusOutlet = outlet_new(&x->x_obj, &s_symbol);	
	
	return (void *)x;
			
}

void serialGPS_free(t_serialGPS *x)
{
	if (x->ogrDS != NULL) serialGPS_writeLog(x);
	
	serialGPS_disconnect(x);

	nmea_parser_destroy(&x->NMEAparser);
}


// *****************************************************************************
extern "C"void serialGPS_setup(void)
{

	OGRRegisterAll(); // we should load only GPX, but load all drivers for now
	
	/*
	for (int i=0; i<OGRSFDriverRegistrar::GetRegistrar()->GetDriverCount(); i++)
	{
		post("Registered OGR driver for %s", OGRSFDriverRegistrar::GetRegistrar()->GetDriver(i)->GetName());
	}
	*/
	
	
	serialGPS_class = class_new(gensym("serialGPS"), (t_newmethod)serialGPS_new,(t_method)serialGPS_free, sizeof(t_serialGPS),CLASS_DEFAULT, A_DEFSYMBOL, 0);

	class_addbang (serialGPS_class, serialGPS_bang);
	class_addmethod (serialGPS_class, (t_method)serialGPS_info, gensym("info"), A_DEFFLOAT, 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_connect, gensym("connect"), A_DEFSYMBOL, A_DEFFLOAT, 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_disconnect, gensym("disconnect"), A_DEFFLOAT, 0);
	class_addmethod(serialGPS_class, (t_method)serialGPS_setDGPS, gensym("setDGPS"), A_DEFFLOAT, 0);
	
	class_addmethod (serialGPS_class, (t_method)serialGPS_setDebug, gensym("setDebug"), A_DEFFLOAT, 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_setThreshold, gensym("setThreshold"), A_DEFFLOAT, 0);


	class_addmethod (serialGPS_class, (t_method)serialGPS_createLog, gensym("createLog"), A_DEFSYMBOL, 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_writeLog, gensym("writeLog"), A_DEFFLOAT, 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_makeWaypoint, gensym("makeWaypoint"), A_GIMME, 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_startTrack, gensym("startTrack"), A_GIMME, 0);
	class_addmethod (serialGPS_class, (t_method)serialGPS_stopTrack, gensym("stopTrack"), A_DEFSYMBOL, 0);

	class_addmethod (serialGPS_class, (t_method)serialGPS_setLoggingMode, gensym("setLoggingMode"), A_DEFFLOAT, 0);

	
	class_addmethod(serialGPS_class, (t_method)serialGPS_sendFloat, gensym("sendFloat"), A_DEFFLOAT, 0);

}
