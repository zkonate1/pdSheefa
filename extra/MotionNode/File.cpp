/**
  Implementation of the File class. See the header file for
  more details.

  @file    tools/sdk/cpp/src/File.cpp
  @author  Luke Tokheim, luke@motionnode.com
  @version 1.2

  (C) Copyright GLI Interactive LLC 2009. All rights reserved.

  The coded instructions, statements, computer programs, and/or related
  material (collectively the "Data") in these files contain unpublished
  information proprietary to GLI Interactive LLC, which is protected by
  US federal copyright law and by international treaties.

  The Data may not be disclosed or distributed to third parties, in whole
  or in part, without the prior written consent of GLI Interactive LLC.

  The Data is provided "as is" without express or implied warranty, and
  with no claim as to its suitability for any purpose.
*/
#include <File.hpp>

#include <stdexcept>


namespace MotionNode { namespace SDK {

File::File(const std::string & pathname)
  : m_input(pathname.c_str(), std::ios_base::binary | std::ios_base::in)
{
  if (!m_input.is_open()) {
    throw std::runtime_error("failed to open input file stream");
  }
}

File::~File()
{
  try {
    close();
  } catch (std::runtime_error &) {
  }
}

void File::close()
{
  if (m_input.is_open()) {
    m_input.close();
  } else {
    throw std::runtime_error("failed to close input file stream, not open");
  }
}

}} // namespace MotionNode::SDK
