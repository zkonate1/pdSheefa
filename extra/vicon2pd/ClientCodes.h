//-----------------------------------------------------------------------------
//	ClientCodes
//-----------------------------------------------------------------------------

// #pragma once

#include <string>
#include <vector>
#include <functional>

class ClientCodes
{
public:
	enum EType
	{
		ERequest,
		EReply
	};

	enum EPacket
	{
		EClose,
		EInfo,
		EData,
		EStreamOn,
		EStreamOff,
		EInfoChange
	};

	static const std::vector< std::string > MarkerTokens;
	static const std::vector< std::string > BodyTokens;
	static const std::vector< std::string > LocalBodyTokens;

	static std::vector< std::string > MakeMarkerTokens()
	{
		std::vector< std::string > v;
		v.push_back("<P-X>");
		v.push_back("<P-Y>");
		v.push_back("<P-Z>");
		v.push_back("<P-O>");
		return v;
	}

	static std::vector< std::string > MakeBodyTokens()
	{
		std::vector< std::string > v;
		v.push_back("<A-X>");
		v.push_back("<A-Y>");
		v.push_back("<A-Z>");
		v.push_back("<T-X>");
		v.push_back("<T-Y>");
		v.push_back("<T-Z>");

		/* Mikael Persson: this was taken out because of competition with LocalBodyTokens.
		//Local tokens
		v.push_back("<ba-X>");
		v.push_back("<ba-Y>");
		v.push_back("<ba-Z>");
		v.push_back("<bt-X>");
		v.push_back("<bt-Y>");
		v.push_back("<bt-Z>");*/

		return v;
	}

	static std::vector< std::string > MakeLocalBodyTokens()
	{
		std::vector< std::string > v;
		v.push_back("<ba-X>");
		v.push_back("<ba-Y>");
		v.push_back("<ba-Z>");
		v.push_back("<bt-X>");
		v.push_back("<bt-Y>");
		v.push_back("<bt-Z>");
		return v;
	}

	struct CompareNames : std::binary_function<std::string, std::string, bool>
	{
		bool operator()(const std::string & a_S1, const std::string & a_S2) const
		{
			std::string::const_iterator iS1 = a_S1.begin();
			std::string::const_iterator iS2 = a_S2.begin();

			while(iS1 != a_S1.end() && iS2 != a_S2.end())
				if(*(iS1++) != *(iS2++)) return false;
				//if(toupper(*(iS1++)) != toupper(*(iS2++))) return false;

			return a_S1.size() == a_S2.size();
		}
	};



};

class MarkerChannel
{
public:
	std::string Name;

	int X;
	int Y;
	int Z;
	int O;

	MarkerChannel(std::string & a_rName) :  Name(a_rName), X(-1), Y(-1), Z(-1), O(-1){}

	int & operator[](int i)
	{
		switch(i)
		{
		case 0:		return X;
		case 1:		return Y;
		case 2:		return Z;
		case 3:		return O;
		default:	/*ASSERT(false);*/ return O;
		}
	}

	int operator[](int i) const
	{
		switch(i)
		{
		case 0:		return X;
		case 1:		return Y;
		case 2:		return Z;
		case 3:		return O;
		default:	/*assert(false);*/ return O;
		}
	}


	bool operator==(const std::string & a_rName)
	{
		ClientCodes::CompareNames comparitor;
		return comparitor(Name, a_rName);
	}

};


class MarkerData
{
public:
	double	X;
	double	Y;
	double	Z;
	bool	Visible;
};

class BodyChannel
{
public:
	std::string Name;

	//Global channels
	int RX;
	int RY;
	int RZ;

	int TX;
	int TY;
	int TZ;

	/* Mikael Persson: this was taken out because of competition with LocalBodyChannel.
	//Local channels
	int tx;
	int ty;
	int tz;
	int rx;
	int ry;
	int rz;*/

	BodyChannel(std::string & a_rName) :  Name(a_rName), RX(-1), RY(-1), RZ(-1), TX(-1), TY(-1), TZ(-1) {}
	//BodyChannel(std::string & a_rName) : RX(-1), RY(-1), RZ(-1), TX(-1), TY(-1), TZ(-1),
	//										rx(-1), ry(-1), rz(-1), tx(-1), ty(-1), tz(-1),Name(a_rName) {}

	int & operator[](int i)
	{
		switch(i)
		{
		case 0:		return RX;
		case 1:		return RY;
		case 2:		return RZ;
		case 3:		return TX;
		case 4:		return TY;
		case 5:		return TZ;/* Mikael Persson: this was taken out because of competition with LocalBodyChannel.
		case 6:		return rx;
		case 7:		return ry;
		case 8:		return rz;
		case 9:		return tx;
		case 10:	return ty;
		case 11:	return tz;*/
		default:	/*assert(false); */ return TZ;
		}
	}

	int operator[](int i) const
	{
		switch(i)
		{
		case 0:		return RX;
		case 1:		return RY;
		case 2:		return RZ;
		case 3:		return TX;
		case 4:		return TY;
		case 5:		return TZ;/* Mikael Persson: this was taken out because of competition with LocalBodyChannel.
		case 6:		return rx;
		case 7:		return ry;
		case 8:		return rz;
		case 9:		return tx;
		case 10:	return ty;
		case 11:	return tz;*/
		default:	/*assert(false); */ return TZ;
		}
	}

	bool operator==(const std::string & a_rName)
	{
		ClientCodes::CompareNames comparitor;
		return comparitor(Name, a_rName);
	}
};

class BodyData
{
public:
	// Representation of body translation
	double	TX;
	double	TY;
	double	TZ;

	/* Mikael Persson: this was taken out because of competition with LocalBodyData.
	//Local body translation
	double tx;
	double ty;
	double tz;*/

	// Angle-Axis
	double	AX;
	double	AY;
	double	AZ;

	/* Mikael Persson: this was taken out because of competition with LocalBodyData.
	//Local angle-axis
	double ax;
	double ay;
	double az;*/

	// Quaternion
	double	QX;
	double	QY;
	double	QZ;
	double	QW;

	/* Mikael Persson: this was taken out because of competition with LocalBodyData.
	//Local Quaternion
	double Qx;
	double Qy;
	double Qz;
	double Qw;*/

	/* Mikael Persson: This was commented out since it is not useful for typical applications.
	// Global rotation matrix
	double GlobalRotation[3][3];

	//Local Rotation matrix
	double LocalRotation[3][3];

	double EulerX;
	double EulerY;
	double EulerZ;

	double Euler_x;
	double Euler_y;
	double Euler_z;
	*/

};

class LocalBodyData
{
public:
	// Representation of body translation
	double	tx;
	double	ty;
	double	tz;

	// Angle-Axis
	double	ax;
	double	ay;
	double	az;


	// Representation of body rotation
	// Quaternion
	double	qx;
	double	qy;
	double	qz;
	double	qw;

	/* Mikael Persson: This was commented out since it is not useful for typical applications.
	// Local rotation matrix
	double LocalRotation[3][3];

	double Euler_x;
	double Euler_y;
	double Euler_z;
	*/

};

class LocalBodyChannel
{
public:
	std::string Name;

	int rx;
	int ry;
	int rz;

	int tx;
	int ty;
	int tz;

	LocalBodyChannel(std::string & a_rName) :  Name(a_rName), rx(-1), ry(-1), rz(-1), tx(-1), ty(-1), tz(-1) {}

	int & operator[](int i)
	{
		switch(i)
		{
		case 0:		return rx;
		case 1:		return ry;
		case 2:		return rz;
		case 3:		return tx;
		case 4:		return ty;
		case 5:		return tz;
		default:	/*assert(false); */ return tz;
		}
	}

	int operator[](int i) const
	{
		switch(i)
		{
		case 0:		return rx;
		case 1:		return ry;
		case 2:		return rz;
		case 3:		return tx;
		case 4:		return ty;
		case 5:		return tz;
		default:	/*assert(false); */ return tz;
		}
	}

	bool operator==(const std::string & a_rName)
	{
		ClientCodes::CompareNames comparitor;
		return comparitor(Name, a_rName);
	}
};
