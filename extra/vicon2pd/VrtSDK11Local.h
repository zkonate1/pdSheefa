//-----------------------------------------------------------------------------
//	Vicon Real Time API.  Error Codes
//
//	Writen By:	Gus Lopez
//	Date:		10/22/01
//	
//	(c)2001 Vicon Motion Systems, Inc.
//
//	Description:  	
//-----------------------------------------------------------------------------
#include "VrtSDK11ErrorCodes.h"
#include <string>
#include <list>

#define MAX_FRAME_RATE 120

using namespace std ;


//Private stream **********************************************
typedef list<double> LISTDOUBLE;


int SocketPrivateHandle;
bool Points, ok;
int pointCount;
int cameraCount;
bool isYup;

LISTDOUBLE pointList;
LISTDOUBLE::iterator pointListIterator;

int privateRtConnect(char *chIpAddr);
int privateRtGetFrame();
int privateRtGetFrameCircles();


bool bIsConnected = VRT_FALSE;
bool mInfoChanged = false;
//int nLastError = 0;

enum ErrorCodes
{
	ConnectFailed,
	NoBodies,
	NoMarkers,
	NotConnected,
	GetFrameFailed
};

long sTime;
long sFrequency;

//-----------------------------------------------------------------------------
//	StatusCodes
//-----------------------------------------------------------------------------

#pragma once

class VStatusCodes
{
public:
	enum ERequestType 
	{	
	ESupportedFlags			= 1<<0,
		ETime					= 1<<1,
		EGeneralStatus			= 1<<2, 
		EReconstructions		= 1<<3,
		EReconPoints			= 1<<4,
		EBodies					= 1<<5,
		EMultiState				= 1<<6,
		ELabelling				= 1<<7,
		EKinematicState			= 1<<8,
		ETVD					= 1<<9,
		EEdges					= 1<<10,
		ECircles				= 1<<11,
		ERawAnalogue			= 1<<12,
		EExtraDiagnostics		= 1<<13,
		ECharacterNames			= 1<<14,
		EStatusFlags			= 1<<15,
		ELabellingByCharacter	= 1<<16,
		ERayAssignments			= 1<<17,
		EVersion				= 1<<18,
		EX2D					= 1<<19,
		EIQVersion				= 1<<20,

		EStartStreaming			= 1<<30,
		EStopStreaming			= 1<<31
	};

	enum EStatusFlags
	{	
		EASAP		= 1<<0
	};
};

class VCircle
{
public:
	float x;
	float y;
	float r;
};

typedef list<VCircle> LISTCIRCLES;

class VCamData2D
{
public:
	int camChannel;
	int numCircles;

	LISTCIRCLES circleList;
	LISTCIRCLES::iterator circleListIterator;

};



//typedef list<VCircle> LISTCIRCLES;
typedef list<VCamData2D> LISTCAMS;
LISTCAMS camList;
LISTCAMS::iterator camListIterator;
