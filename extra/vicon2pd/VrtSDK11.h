//-----------------------------------------------------------------------------
//	Vicon Real Time API.  Include file
//
//	Writen By:	Gus Lopez
//	Date:		10/22/01
//	
//	(c)2001 Vicon Motion Systems, Inc.
//
//	Description:  	
//----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Aerospace Mechatronics Laboratory,
// McGill University, Montreal, Canada
// 
// Modified By: Mikael Persson
// Date:		08/14/2007
//
// Modifications:
//		- Changed all return values to int instead of bool, this enables (possibly) more relevant
//		  error codes and makes more sense for a .dll that should pass variables to the host
//		  application through the registry (32bit values are more efficient). I know this is not
//		  ideal for a 64bit processor, but this compatibility could be added latter.
//		- Added Capabilities to get the marker, body, local body positions through
//		  an index reference to prevent useless and very unefficient string finding procedures.
//		- Changed the redundant definition of Bodies and Local Bodies (whatever they are?) because
//		  the same channels were defined for both bodies and local bodies which will inevitably
//		  cause problems, redundancy, or duplicates. It is preferable to just have two seperate
//		  arrays since it does not make any difference in performance, but it's safer.
// Remarks:
//		- The variable isYup seems completely crazy to me... This actually does a 90 degrees
//		  rotation which changes the global frame (and wrongfully does so, btw). If anyone wants to do 
//		  that they can do it them-selves... I kept it because of backward compatibility?
//-----------------------------------------------------------------------------

#define VRTSDK11_API 


/* Mikael Persson: This is useless (and somewhat dangerous), the private functions are already protected by the EXTERNAL define.
//#ifdef PRIVATEVICONRT_EXPORTS
#define PRIVATEVICONRT_API __declspec(dllexport)
//#else
//#define PRIVATEVICONRT_API __declspec(dllimport)
//#endif
*/

#define VRT_ERRF_CONNECT	0x00010000
VRTSDK11_API int ViconConnect(char *chIpAddr);

VRTSDK11_API int ViconHasDataChanged();

#define VRT_ERRF_CLOSE		0x00020000
VRTSDK11_API int ViconClose();

#define VRT_ERRF_GETFRAME	0x00030000
#ifndef EXTERNAL
VRTSDK11_API int ViconGetFrame(int getUnlabeled, int getCircles);
#else
VRTSDK11_API int ViconGetFrame(int getUnlabeled);
#endif

VRTSDK11_API int ViconIsConnected();

VRTSDK11_API void ViconGetLastError(char *chError);

#define VRT_ERRF_GETFRAMETIMESTAMP	0x00040000
VRTSDK11_API int ViconGetFrameTimeStamp(double *dTimeStamp);


#define VRT_ERRF_GETNUMMARKERS	0x00050000
VRTSDK11_API int ViconGetNumMarkers( int *markerNum );

#define VRT_ERRF_GETMARKERNAME	0x00060000
VRTSDK11_API int ViconGetMarkerName( int markerNum, char *name );

#define VRT_ERRF_GETMARKERBYINDEX	0x00070000
VRTSDK11_API int ViconGetMarkerByIndex( int markerNum, float *a_rX, float *a_rY, float *a_rZ, int *a_rV );

#define VRT_ERRF_GETMARKERBYNAME	0x00080000
VRTSDK11_API int ViconGetMarkerByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, int *a_rV );


#define VRT_ERRF_GETNUMBODIES	0x00090000
VRTSDK11_API int ViconGetNumBodies( int *bodyNum );

#define VRT_ERRF_GETBODYNAME	0x000A0000
VRTSDK11_API int ViconGetBodyName( int bodyNum, char *name );

#define VRT_ERRF_GETBODYANGLEAXISBYINDEX	0x000B0000
VRTSDK11_API int ViconGetBodyAngleAxisByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ );

#define VRT_ERRF_GETBODYQUATERNIONBYINDEX	0x000C0000
VRTSDK11_API int ViconGetBodyQuaternionByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rQX, float *a_rQY, float *a_rQZ, float *a_rQW);

#define VRT_ERRF_GETBODYEULERANGLESBYINDEX	0x000D0000
VRTSDK11_API int ViconGetBodyEulerAnglesByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ);

#define VRT_ERRF_GETBODYROTATIONMATRIXBYINDEX	0x000E0000
VRTSDK11_API int ViconGetBodyRotationMatrixByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float a_rGlobalRotationMatrix[3][3]);

#define VRT_ERRF_GETBODYANGLEAXISBYNAME	0x000F0000
VRTSDK11_API int ViconGetBodyAngleAxisByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ );

#define VRT_ERRF_GETBODYQUATERNIONBYNAME	0x00100000
VRTSDK11_API int ViconGetBodyQuaternionByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rQX, float *a_rQY, float *a_rQZ, float *a_rQW);

#define VRT_ERRF_GETBODYEULERANGLESBYNAME	0x00110000
VRTSDK11_API int ViconGetBodyEulerAnglesByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ);

#define VRT_ERRF_GETBODYROTATIONMATRIXBYNAME	0x00120000
VRTSDK11_API int ViconGetBodyRotationMatrixByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float a_rGlobalRotationMatrix[3][3]);


#define VRT_ERRF_GETNUMLOCALBODIES	0x00130000
VRTSDK11_API int ViconGetNumLocalBodies( int *bodyNum );

#define VRT_ERRF_GETLOCALBODYNAME	0x00140000
VRTSDK11_API int ViconGetLocalBodyName( int bodyNum, char *name );

#define VRT_ERRF_GETLOCALBODYANGLEAXISBYINDEX	0x00150000
VRTSDK11_API int ViconGetLocalBodyAngleAxisByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ);

#define VRT_ERRF_GETLOCALBODYQUATERNIONBYINDEX	0x00160000
VRTSDK11_API int ViconGetLocalBodyQuaternionByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rQX, float *a_rQY, float *a_rQZ, float *a_rQW);

#define VRT_ERRF_GETLOCALBODYEULERANGLESBYINDEX	0x00170000
VRTSDK11_API int ViconGetLocalBodyEulerAnglesByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ);

#define VRT_ERRF_GETLOCALBODYROTATIONMATRIXBYINDEX	0x00180000
VRTSDK11_API int ViconGetLocalBodyRotationMatrixByIndex( int bodyNum, float *a_rX, float *a_rY, float *a_rZ, float a_rGlobalRotationMatrix[3][3]);

#define VRT_ERRF_GETLOCALBODYANGLEAXISBYNAME	0x00190000
VRTSDK11_API int ViconGetLocalBodyAngleAxisByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ);

#define VRT_ERRF_GETLOCALBODYQUATERNIONBYNAME	0x001A0000
VRTSDK11_API int ViconGetLocalBodyQuaternionByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rQX, float *a_rQY, float *a_rQZ, float *a_rQW);

#define VRT_ERRF_GETLOCALBODYEULERANGLESBYNAME	0x001B0000
VRTSDK11_API int ViconGetLocalBodyEulerAnglesByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float *a_rEX, float *a_rEY, float *a_rEZ);

#define VRT_ERRF_GETLOCALBODYROTATIONMATRIXBYNAME	0x001C0000
VRTSDK11_API int ViconGetLocalBodyRotationMatrixByName( char * a_pName, float *a_rX, float *a_rY, float *a_rZ, float a_rGlobalRotationMatrix[3][3]);


VRTSDK11_API void ViconSetYup();

VRTSDK11_API void ViconResetYup();


//Private stream***********************************************

#define VRT_ERRF_PRIVATEGETPOINTCOUNT	0x001D0000
VRTSDK11_API int privateRtGetPointCount();

#define VRT_ERRF_PRIVATEGETNEXTPOINT	0x001E0000
VRTSDK11_API int privateRtGetNextPoint(double point[3]);



#ifndef EXTERNAL

#define VRT_ERRF_PRIVATEGETCAMERACOUNT	0x001F0000
VRTSDK11_API int privateRtGetCameraCount();

#define VRT_ERRF_PRIVATEGETCAMERACHANNELCIRCLECOUNT	0x00200000
VRTSDK11_API int privateRtGetCameraChannelCircleCount(int camChannel);

#define VRT_ERRF_PRIVATEGETCAMERACHANNELCIRCLE	0x00210000
VRTSDK11_API int privateRtGetCameraChannelCircle(int camChannel, int circleNum, float *x, float *y, float *r);

#define VRT_ERRF_PRIVATEGETCAMERACHANNELTARSUSCHANNELNUM	0x00220000
VRTSDK11_API int privateRtGetCameraChannelTarsusChannelNum(int camChannel);
#endif
