// TestApp.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include "VrtSDK11.h"
#include "VrtSDK11ErrorCodes.h"

int main(int argc, char* argv[])
{
//	char temp[16]; NOT USED - by Marcello Stani
	int nNumBodies = 0;
	int nNumMarkers = 0;
	char name[80];
	float X;
	float Y;
	float Z;
	float rX;
	float rY;
	float rZ;
	float rW;

	int err;

	double dTimestamp;

	int V;

	printf("\n\nVicon Real Time API Test Application\n");

	printf("\nConnecting to server %s\n", argv[1]);
	err = ViconConnect(argv[1]);
	if(err == VRT_OK)
		printf("\nConnected ok");
	else
		printf("\n\nFAILED to connect with server with error %d", err);

	if (ViconIsConnected())
		printf("\nViconIsConnected returned TRUE\n");
	else {
	  printf ("\n\nConnection failed... terminating\n\n");
	  exit(0);
	}

// Retrieving number of bodies
	err = ViconGetNumBodies(&nNumBodies);
	if ( err != VRT_OK )
	{
	  printf ("Error on number of bodies:\n");
	}

	printf("\nNumber of bodies: %d", nNumBodies);

// Retrieving number of markers
	err = ViconGetNumMarkers(&nNumMarkers );
	if ( err != VRT_OK )
	{
	  printf ("Error on number of markers:\n");
	}

	printf("\nNumber of Markers: %d", nNumMarkers);

	printf("\nMarkers\t\t\tBodies\n");
	for (int i = 0; i < nNumMarkers; i++)
	{
		//Display markers
		ViconGetMarkerName( i, name );
		printf("\n%s",name);
	}

	for (int i = 0; i < nNumBodies; i++)
	{
		//Display bodies
		ViconGetBodyName( i, name );
		printf("\n%s", name);
	}

	//Get and display a frame of data;
	for (int J = 0; J <= 10; J++)
	{
		//Get data
#ifndef EXTERNAL
		ViconGetFrame(0,0);
#else
		ViconGetFrame(0);
#endif

#ifdef FULL
		//Display Time stamp
		ViconGetFrameTimeStamp(&dTimestamp);
		printf("\n\nTimeStamp:  %.0f\n", dTimestamp);
		//Display all marker data for current frame
		printf("\nMarkers: ");
#endif
		for (int I = 0; I < nNumMarkers; I++)
		{
			ViconGetMarkerName( I, name );
			ViconGetMarkerByName( name, &X, &Y, &Z, &V );
			printf("\n%s: %.2f, %.2f, %.2f, %.2f", name, X, Y, Z, V);
		}

#ifdef FULL
		//Display all body data for current frame
		printf("\n\nBodies: ");
		for (int b = 0; b < nNumBodies; b++)
		{
			ViconGetBodyName( b, name );

			//Display Quaternion
			ViconGetBodyQuaternionByName( name, &X, &Y, &Z, &rX, &rY, &rZ, &rW );
			printf("\nQuaternions %s: %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f", name, X, Y, Z, rX, rY, rZ, rW);
		}
		printf("\n\n");
#endif
	}

	printf("Closing connection with Vicon server...");

	ViconClose();
	return 0;
}
