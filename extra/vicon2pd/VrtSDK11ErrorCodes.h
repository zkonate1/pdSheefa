//-----------------------------------------------------------------------------
//	Vicon Real Time API.  Error Codes
//
//	Writen By:	Gus Lopez
//	Date:		10/22/01
//	
//	(c)2001 Vicon Motion Systems, Inc.
//
//	Description:  	
//-----------------------------------------------------------------------------

#define VRT_OK				0x00000000
#define VRT_FAIL			0x00000001
#define VRT_INVALIDARG		0x00000002
#define VRT_FAILED_REQUEST	0x00000003
#define VRT_FAILED_REPLY	0x00000004
#define VRT_WRONG_REPLY		0x00000005
#define VRT_CORRUPT_DATA	0x00000006
#define VRT_NOT_CONNECTED	0x00000007

#define VRT_TRUE	1
#define VRT_FALSE	0



