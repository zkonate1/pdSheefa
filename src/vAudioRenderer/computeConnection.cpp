#include <vector>

#include "vMath.h"
#include "vAudioManager.h"
#include "vSoundNode.h"
#include "vSoundConn.h"

using namespace std;

// input: bang
// creation args: < connSymbol A E D >  (note: A,E,D of the SINK!)
// outlet 1: incidenceGain
// outlet 2: incidenceFilter (hz)

extern vAudioManager *globalAudioManager;


class sinkOffset
{
public:
	sinkOffset(int _id) { id=_id; }
	~sinkOffset() {};
	int id;
	Vector3 posOffset;
	Vector3 rotOffset;
	bool atOrigin;
};


typedef std::vector<sinkOffset*>::iterator sinkIterator;

// *****************************************************************************
// declaration of this Pd class:
t_class *computeConnection_class;

// declaration of the pd struct:
typedef struct _computeConnection
{
	t_object x_obj;
  
	vSoundConn *conn;
	
	std::vector<sinkOffset*> sinks;
	std::vector<sinkOffset*> originSinks; // vec of no offset (subset of sinks)
	
	int numOriginSinks;
	
 	// keep atom list in memory so not to re-allocate with each computation
 	t_atom coeffAtoms[6];
	
 	t_outlet *coeff_outlet;
 	t_outlet *muting_outlet;

} t_computeConnection;


#define GAIN_THRESHOLD 0.004
// note:  as soon as threshold is crossed by any one of the connected sinks, muting is enabled/disabled

// *****************************************************************************
static void computeConnection_bang(t_computeConnection *x)
{
	if (!x->conn) return;

	// common distance & srcIncidence for sinks at the origin:
	Vector3 g_vect;
	double g_distance, g_distanceScalar, g_srcIncidenceGain;
	
	if (x->numOriginSinks)
	{
		g_vect = x->conn->snk->pos - x->conn->src->pos;
			
		// DISTANCE:
		g_distance = (double)g_vect.Mag();
		g_distanceScalar = 1 / (1.0 + pow(g_distance,(double)x->conn->distanceEffect*.01));
				
		// SIMPLE MUTE MECHANISM (TODO: also mute any connected regions)
		
		if (g_distanceScalar >= GAIN_THRESHOLD && x->conn->mute)
		{
			x->conn->mute = false;
			outlet_float(x->muting_outlet, (t_float) 0.);
		}

		else if (g_distanceScalar < GAIN_THRESHOLD && !x->conn->mute )
		{
			x->conn->mute = true;
			outlet_float(x->muting_outlet, (t_float) 1.);
			return;
		}

		
		// SRC INCIDENCE:
		Vector3 srcDir = EulerToQuat(x->conn->src->rot) * Vector3(0,1,0);
		double srcIncidence = AngleBetweenVectors(srcDir, g_vect);
		g_srcIncidenceGain = x->conn->src->rolloff->getValue( (srcIncidence * x->conn->src->spread) / M_PI );
		
		
	}
	
	
	for (sinkIterator s = x->sinks.begin(); s != x->sinks.end(); s++)
	{
		Vector3 vect;
		double distance, distanceScalar, srcIncidenceGain;
		
		if ((*s)->atOrigin)
		{
			vect = g_vect;
			distance = g_distance;
			distanceScalar = g_distanceScalar;
			srcIncidenceGain = g_srcIncidenceGain;
		}
		
		else
		{
			vect = x->conn->snk->pos + (*s)->posOffset - x->conn->src->pos;
			
			// DISTANCE:
			distance = (double)vect.Mag();
			distanceScalar = 1 / (1.0 + pow(distance,(double)x->conn->distanceEffect*.01));
			
			if (distanceScalar >= GAIN_THRESHOLD && x->conn->mute)
			{
				x->conn->mute = false;
				outlet_float(x->muting_outlet, (t_float) 0.);
			}
			
			else if (distanceScalar < GAIN_THRESHOLD && !x->conn->mute )
			{
				x->conn->mute = true;
				outlet_float(x->muting_outlet, (t_float) 1.);
				return;
			}

			// SRC INCIDENCE:
			Vector3 srcDir = EulerToQuat(x->conn->src->rot) * Vector3(0,1,0);
			double srcIncidence = AngleBetweenVectors(srcDir, vect);
			srcIncidenceGain = x->conn->src->rolloff->getValue( (srcIncidence * x->conn->src->spread) / M_PI );
			
		}

		// SNK INCIDENCE:
		Vector3 snkDir = EulerToQuat(x->conn->snk->rot + (*s)->rotOffset) * Vector3(0,1,0);
		double snkIncidence = AngleBetweenVectors(Vector3(0,0,0)-snkDir, vect);
		double snkIncidenceGain = x->conn->snk->rolloff->getValue( (snkIncidence * x->conn->snk->spread) / M_PI );
		
		// final incidence scalars with connection's rolloffEffect applied:
		double srcScalar = (double) (1.0 - (.01*x->conn->rolloffEffect  * (1.0 - srcIncidenceGain)));
		double snkScalar = (double) (1.0 - (.01*x->conn->rolloffEffect  * (1.0 - snkIncidenceGain)));
		
		
		double diffractionScaler = (double) (1.0 - (.01*x->conn->diffractionEffect  * (1.0 - srcIncidenceGain*snkIncidenceGain)));
		
		
		// incidence filter:
		// - range is 500Hz -> 21750 + 500 Hz)
		// - feeds Pd object [lop~ 500]
		double Lpf1 = 500 + ( 21550 * (0.5 - ( .5*cos(pow(diffractionScaler,4)*M_PI) ) ) );
		
		// absorption filter:
		// - function of distance
		// - range is 50Hz -> 22000Hz
		// - feeds Pd object [lop~ 22000]
		double Lpf2 = 22000 - ( 3500*log(distance+1) );
		if (Lpf2 < 50) Lpf2=50;
		if (Lpf2 > 22000) Lpf2=22000;
		
		// variable delay for Doppler:
		// - feeds Pd object [vd~ $0-dop]
		double vdel1 = distance * 2.89855 * .01 * x->conn->dopplerEffect;  // speed of sound
		
		
	
		
		// Now output coeffs:
		SETFLOAT (x->coeffAtoms+0, (t_float) (*s)->id);
		SETFLOAT (x->coeffAtoms+1, (t_float) snkScalar);
		SETFLOAT (x->coeffAtoms+2, (t_float) Lpf1);
		SETFLOAT (x->coeffAtoms+3, (t_float) srcScalar*distanceScalar);
		SETFLOAT (x->coeffAtoms+4, (t_float) Lpf2);
		SETFLOAT (x->coeffAtoms+5, (t_float) vdel1);		
		//outlet_anything(x->coeff_outlet, x->conn->id, 5, x->coeffAtoms);
		outlet_anything(x->coeff_outlet, &s_list, 6, x->coeffAtoms);

		
	}
}

// *****************************************************************************
void computeConnection_addSink (t_computeConnection *x, t_symbol *s, int argc, t_atom *argv)
//void computeConnection_addSink (t_computeConnection *x, t_floatarg _id, t_floatarg _x, t_floatarg _y, t_floatarg _z, t_floatarg _pitch, t_floatarg _roll, t_floatarg _yaw)
{
	// check args:
	bool validArgs = true;
	if (argc!=7) validArgs = false;
	for (int i=0; i<argc; i++)
	{
		if (argv[i].a_type!=A_FLOAT) validArgs = false;
		break;
	}
	if (!validArgs)
	{
		post("[computeConnection] ERROR: addSink method takes the following list: < id x y z pitch roll yaw >");
		return;
	}
	
	
	sinkOffset *theSink = NULL;
	
	int _id = (int)atom_getfloatarg(0, argc, argv);
	
	// See if the sink already exists. If so, just update that one:
	for (sinkIterator s = x->sinks.begin(); s != x->sinks.end(); s++)
	{
		if ((*s)->id == _id) theSink = (*s);
	}
	
	// Create a new sink if the id wasn't found:
	if (!theSink)
	{
		theSink = new sinkOffset(_id);
		x->sinks.push_back(theSink);
		if (theSink->posOffset.Mag()==0)
		{
			theSink->atOrigin = true;
			x->numOriginSinks++;
		}
	}

	// Update the offsets:
	theSink->posOffset.x = atom_getfloatarg(1, argc, argv);
	theSink->posOffset.y = atom_getfloatarg(2, argc, argv);
	theSink->posOffset.z = atom_getfloatarg(3, argc, argv);
	theSink->rotOffset.x = atom_getfloatarg(4, argc, argv);
	theSink->rotOffset.y = atom_getfloatarg(5, argc, argv);
	theSink->rotOffset.z = atom_getfloatarg(6, argc, argv);
	
	// check if sink atOrigin state has changed and adjust reference count:
	if (theSink->posOffset.Mag()==0)
	{
		if (!theSink->atOrigin)
		{
			theSink->atOrigin = true;
			x->numOriginSinks++;
		}
	} else {
		if (theSink->atOrigin)
		{
			theSink->atOrigin = false;
			x->numOriginSinks--;
		}
	}
}

void computeConnection_removeSink (t_computeConnection *x, t_floatarg _id)
{
	for (sinkIterator s = x->sinks.begin(); s != x->sinks.end(); s++)
	{
		if ((*s)->id == (int)_id)
		{
			if ((*s)->atOrigin) x->numOriginSinks--;
			delete (*s);
			x->sinks.erase(s);
			break;
		}
	}
}

void computeConnection_setConnection (t_computeConnection *x, t_symbol *s)
{
	if (globalAudioManager)
	{
		x->conn = globalAudioManager->getConnection(s);
		if (!x->conn) post("[computeConnection] warning: '%s' doesn't exist", s->s_name);
	}
	else post("ERROR: globalAudioManager doesn't exist.");
	
	
}

void computeConnection_debug (t_computeConnection *x)
{
	post("\n=====================================================");
	if (x->conn)
	{
		post("[computeConnection] for %s: %d sinks (%d with no offset)", x->conn->id->s_name, x->sinks.size(), x->numOriginSinks);
		for (sinkIterator s = x->sinks.begin(); s != x->sinks.end(); s++)
		{
			post("  sink %d posOffset: (%.3f,%.3f,%.3f)",(*s)->id,(*s)->posOffset.x,(*s)->posOffset.y,(*s)->posOffset.z);
			post("  sink %d rotOffset: (%.3f,%.3f,%.3f)",(*s)->id,(*s)->rotOffset.x,(*s)->rotOffset.y,(*s)->rotOffset.z);
		}
	}
	
	else {
		post("[computeConnection] symbol not set. Use the setConnection method.");
	}
}

void *computeConnection_new (t_symbol *s, int argc, t_atom *argv)
{	
	t_computeConnection *x = (t_computeConnection *) pd_new (computeConnection_class);
	
	if (argc && (argv[0].a_type==A_SYMBOL))
	{
		computeConnection_setConnection(x, argv[0].a_w.w_symbol);
	}
	else
	{
		post("ERROR: [computeConnection] takes either no arguments or a connection symbol.");
		return NULL;	
	}
	
	x->numOriginSinks = 0;
	

	// simulate a message to make one initial sink (with id 0, and no offset):
	//computeConnection_addSink( x, 0, 0,0,0, 0,0,0 );
	int atomCount = 0;
	t_atom *atoms = atomsFromString("0 0 0 0 0 0 0", atomCount);
	computeConnection_addSink(x, gensym("addSink"), atomCount, atoms);
	
	
	x->coeff_outlet = outlet_new(&x->x_obj, &s_list);
	x->muting_outlet = outlet_new(&x->x_obj, &s_float);

	return (void *) x;
}

void computeConnection_free(t_computeConnection *x)
{	
	sinkIterator s = x->sinks.begin();
	while (s != x->sinks.end())
	{
		delete (*s);
		x->sinks.erase(s);
	}
	
	x->numOriginSinks = 0;
}

extern "C" void computeConnection_setup(void)
{
	computeConnection_class = class_new(gensym("computeConnection"), (t_newmethod)computeConnection_new, (t_method)computeConnection_free, sizeof(t_computeConnection), 0, A_GIMME, 0);
	class_addbang(computeConnection_class, (t_method) computeConnection_bang);
	class_addmethod(computeConnection_class, (t_method) computeConnection_setConnection, gensym("setConnection"), A_SYMBOL, 0);
	//class_addmethod(computeConnection_class, (t_method) computeConnection_addSink, gensym("addSink"), A_DEFFLOAT, A_DEFFLOAT, A_DEFFLOAT, A_DEFFLOAT, A_DEFFLOAT, A_DEFFLOAT, A_DEFFLOAT, 0);
	class_addmethod(computeConnection_class, (t_method) computeConnection_addSink, gensym("addSink"), A_GIMME, 0);
	class_addmethod(computeConnection_class, (t_method) computeConnection_removeSink, gensym("removeSink"), A_DEFFLOAT, 0);
	class_addmethod(computeConnection_class, (t_method) computeConnection_debug, gensym("debug"), A_DEFFLOAT, 0);
}

