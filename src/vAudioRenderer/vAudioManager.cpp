#include "vAudioManager.h"
#include "vListener.h"
#include "vSoundNode.h"
#include "vSoundConn.h"
#include "vRolloffTable.h"

using namespace std;


// *****************************************************************************
// constructor
vAudioManager::vAudioManager ()
{
	this->vListenerList.clear();
	this->vSoundNodeList.clear();
	this->vSoundConnList.clear();
	this->vRolloffList.clear();
}

// *****************************************************************************
// destructor
vAudioManager::~vAudioManager()
{
	listenerIterator L = vListenerList.begin();
	while (L != vListenerList.end())
	{
		delete (*L);
		vListenerList.erase(L);
	}
	nodeIterator N = vSoundNodeList.begin();
	while (N != vSoundNodeList.end())
	{
		delete (*N);
		vSoundNodeList.erase(N);
	}
	connIterator C = vSoundConnList.begin();
	while (C != vSoundConnList.end())
	{
		delete (*C);
		vSoundConnList.erase(C);
	}
	rolloffIterator R = vRolloffList.begin();
	while (R != vRolloffList.end())
	{
		delete (*R);
		vRolloffList.erase(R);
	}
}


// *****************************************************************************
// This is a function that can be used by std::sort to make a
// list of nodes alphabetical:
static bool nodeSortFunction (vSoundNode *n1, vSoundNode *n2)
{
	return ( string(n1->id->s_name) < string(n2->id->s_name) );
}

// *****************************************************************************
// return a pointer to a vListener in the vListenerList, given an id:
vListener* vAudioManager::getListener(t_symbol *id)
{
	listenerIterator L;
	for (L = vListenerList.begin(); L != vListenerList.end(); L++)
	{
		if ((*L)->node->id == id)
		{
			return (*L);
		}
	}
	
	return NULL;
}

// *****************************************************************************
// return a pointer to a vSoundNode in the vSoundNodeList, given an id:
vSoundNode* vAudioManager::getNode(t_symbol *id)
{
	nodeIterator n;
	for (n = vSoundNodeList.begin(); n != vSoundNodeList.end(); n++)
	{
		if ((*n)->id == id)
		{
			return (*n);
		}
	}
	
	return NULL;
}

// *****************************************************************************
// return a list of all connections that "directly involve" a node (ie, as the
// source or the sink):
std::vector<vSoundConn*> vAudioManager::getConnectionsForNode(t_symbol *id)
{
	vector<vSoundConn*> foundConnections;
	
	connIterator c;
	for (c = vSoundConnList.begin(); c != vSoundConnList.end(); c++)
	{
		if (((*c)->src->id == id) || ((*c)->snk->id == id))
		{
			foundConnections.push_back(*c);
		}
	}
	return foundConnections;
}

// *****************************************************************************
// return a pointer to a vSoundConn in the vSoundConnList:
vSoundConn* vAudioManager::getConnection(t_symbol *src, t_symbol *snk)
{

	connIterator c;
	for (c = vSoundConnList.begin(); c != vSoundConnList.end(); c++)
	{
		if (((*c)->src->id == src) && ((*c)->snk->id == snk))
		{
			return (*c);
		}
	}
	
	
	/*
	vSoundNode *srcNode = getNode(x,src);
	if ( srcNode )
	{
		connIterator c;
		for (c = srcNode->connectTO.begin(); c != srcNode->connectTO.end(); c++)
		{
			if ((*c)->snk->id == snk) return (*c);
		}
	}
	*/

	return NULL;
}

vSoundConn* vAudioManager::getConnection(t_symbol *id)
{
	connIterator c;
	for (c = vSoundConnList.begin(); c != vSoundConnList.end(); c++)
	{
		if ((*c)->id == id)
		{
			return (*c);
		}
	}
	
	return NULL;
}



vRolloffTable* vAudioManager::getRolloff (t_symbol *id)
{
	rolloffIterator r;
	for (r = vRolloffList.begin(); r != vRolloffList.end(); r++)
	{
		if ((*r)->tableName == id)
		{
			return (*r);
		}
	}

	return NULL;
	
}



