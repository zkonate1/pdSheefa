

static void vAudioRenderer_refreshGraph(t_vAudioRenderer *x)
{
	listenerIterator L;
	
	for (L = x->vListenerList.begin(); L != x->vListenerList.end(); L++)
	{
		/*
		if ( (*L)->node->connectFROM.size() )
		{
			(*L)->node->refCount=1;
			(*L)->node->createPdObj(x);
		} else {
			(*L)->node->refCount=0;
			(*L)->node->deletePdObj(x);
		}
		*/
		(*L)->node->createPdObj(x);
		vAudioRenderer_refreshGraph(x, *L, (*L)->node);
	}
}

static void vAudioRenderer_incRefCount(t_vAudioRenderer *x, vSoundNode *n)
{
	connIterator c;
	for (c = n->connectFROM.begin(); c != n->connectFROM.end(); c++)
	{
		// create the source node if it doesn't exist:
		if (!(*c)->src->refCount) (*c)->src->createPdObj(x);			
		(*c)->src->refCount++;
		
		(c)
		
		vAudioRenderer_incRefCount(x, (*c)->src);
	}
	
}
	
static void vAudioRenderer_refreshGraph(t_vAudioRenderer *x, vListener *listener, vSoundNode *sinkNode)
{
	connIterator c;
	for (c = sinkNode->connectFROM.begin(); c != sinkNode->connectFROM.end(); c++)
	{

		// if the listener doesnt exist as a reference, add it:
		if (find((*c)->src->references.begin(), (*c)->src->references.end(), listener) == (*c)->src->references.end())
		{
			// if this is the first listener to be added, make sure that the Pd
			// patches exist:
			if (!(*c)->src->references.size()) (*c)->src->createPdObj(x);
			(*c)->src->references.push_back(listener);
		}
		// same for the connection:
		if (find((*c)->references.begin(), (*c)->references.end(), listener) == (*c)->references.end())
		{
			if (!(*c)->references.size()) (*c)->createPdObj(x);
			(*c)->references.push_back(listener);
		}

		// recursively refresh rest of graph:
		vAudioRenderer_refreshGraph(x, listener, (*c)->src);
	}
		
	/*	
	connIterator c;
	for (c = sinkNode->connectFROM.begin(); c != sinkNode->connectFROM.end(); c++)
	{
		// increment refCount for the source:
		// (only if listener doesn't already know about it)
		if (find(listener->refNodes.begin(), listener->refNodes.end(), (*c)->src) == listener->refNodes.end())
		{
			// create the source node if it doesn't exist:
			if (!(*c)->src->refCount) (*c)->src->createPdObj(x);
			
			(*c)->src->refCount++;
			listener->refNodes.push_back((*c)->src);
		}
		
	
		// increment refCount for the connection:
		// (only if listener doesn't already know about it)
		if (find(listener->refConns.begin(), listener->refConns.end(), *c) == listener->refConns.end())
		{
			// create connection pd patch if it doesn't exist:
			if (!(*c)->refCount) (*c)->createPdObj(x);
			
			(*c)->refCount++;
			listener->refConns.push_back(*c);
		}
		
		// recursively refresh rest of graph:
		vAudioRenderer_refreshGraph(x, listener, (*c)->src);
	}
	*/
}


static void vAudioRenderer_disableNode(t_vAudioRenderer *x, vSoundNode *node)
{
	for (connIterator c = node->connectFROM.begin(); c != node->connectFROM.end(); c++)
	{
		vAudioRenderer_disableConn(t_vAudioRenderer *x, vSoundConn *conn);
	}
	
	node->deletePdObj(x);
	node->references.clear();
}

static void vAudioRenderer_disableConn(t_vAudioRenderer *x, vSoundConn *conn)
{
	vAudioRenderer_disableNode(x, conn->src);
	
	conn->deletePdObj(x);
	conn->references.clear();
}
	
/*
static void vAudioRenderer_disableNode(t_vAudioRenderer *x, vListener *listener, vSoundNode *node)
{
	nodeIterator n = find(listener->refNodes.begin(), listener->refNodes.end(), node);
	if (n!=listener->refNodes.end())
	{
		listener->refNodes.erase(n);
		node->refCount--;
		if (node->refCount < 1) node->deletePdObj(x);
	}
}

static void vAudioRenderer_disableNode(t_vAudioRenderer *x, vSoundNode *node)
{
	listenerIterator L;
	for (L = x->vListenerList.begin(); L != x->vListenerList.end(); L++)
	{
		vAudioRenderer_disableNode(x, (*L), node);
	}
}
	
static void vAudioRenderer_disableConn(t_vAudioRenderer *x, vListener *listener, vSoundConn *conn)
{
	connIterator c = find(listener->refConns.begin(), listener->refConns.end(), conn);
	if (c!=listener->refConns.end())
	{
		listener->refConns.erase(c);
		conn->refCount--;
		if (conn->refCount < 1) conn->deletePdObj(x);
	}
}

static void vAudioRenderer_disableConn(t_vAudioRenderer *x, vSoundConn *conn)
{
	listenerIterator L;
	for (L = x->vListenerList.begin(); L != x->vListenerList.end(); L++)
	{
		vAudioRenderer_disableConn(x, (*L), conn);
	}
}

static void vAudioRenderer_disableGraph(t_vAudioRenderer *x, vListener *listener, vSoundConn *conn)
{
	// recursively disable all upstream nodes and connections:
	for (connIterator c = conn->src->connectFROM.begin(); c != conn->src->connectFROM.end(); c++)
	{
		vAudioRenderer_disableGraph(x, listener, *c);
		vAudioRenderer_disableNode(x, listener, (*c)->src);
		vAudioRenderer_disableConn(x, listener, *c);
	}
}

static void vAudioRenderer_disableGraph(t_vAudioRenderer *x, vSoundConn *conn)
{
	listenerIterator L;
	for (L = x->vListenerList.begin(); L != x->vListenerList.end(); L++)
	{
		if ( find((*L)->refConns.begin(), (*L)->refConns.end(), conn) != (*L)->refConns.end() )
		{
			vAudioRenderer_disableGraph(x, (*L), conn);
		}
	}
}
*/

/*
// sorting function
struct vSoundConn_compare
{
	bool operator()(const vSoundConn & c1, const vSoundConn & c2) const
	{
		if (c1.id->s_name < c2.id->s_name)
			return true;
		else
			return false;
	}
}
*/

// *****************************************************************************
/**
 * return a list of all connections that feed a sink node with sound, either
 * directly or recursively through chained connections:
 */
/*
static std::set<vSoundConn*> getConnectedGraph(t_vAudioRenderer *x, vSoundNode *sinkNode)
{
	set<vSoundConn*,vSoundConn_compare> connectedGraph;
	
	connIterator c;
	for (c = sinkNode->connectFROM.begin(); c != sinkNode->connectFROM.end(); c++)
	{
		set<vSoundConn*,vSoundConn_compare> tmp1 = getConnectedGraph(x,(*c)->src); // recursive
		set<vSoundConn*,vSoundConn_compare> tmp2 = connectedGraph;
		std::merge(tmp1.begin(), tmp1.end(), tmp2.begin(), tmp2.end(), connectedGraph );
		connectedGraph.insert(*c);
	}
	
	return connectedGraph;
}
*/

/*
static std::vector<vSoundConn*> getConnectedGraph(t_vAudioRenderer *x, vSoundNode *sinkNode)
{
	vector<vSoundConn*> connectedGraph;
	
	connIterator c;
	for (c = sinkNode->connectFROM.begin(); c != sinkNode->connectFROM.end(); c++)
	{
		vector<vSoundConn*> tmp = getConnectedGraph(x,(*c)->src); // recursive
		connectedGraph.insert( connectedGraph.end(), tmp.begin(), tmp.end() );
		connectedGraph.push_back(*c);
	}
	
	return connectedGraph;
}
*/

	
	/*
	// keeping a list of references instead of simple refCount
	
	connIterator c;
	for (c = sinkNode->connectFROM.begin(); c != sinkNode->connectFROM.end(); c++)
	{
		*vSoundConn conn = (*c);
 		
		// create the source node (assume the sink already exists):
		if (!conn->src->references->length()) conn->src->createPdObj(x);
		
		// ensure the source has this listener in it's list of references:
		if (find(conn->src->references.begin(), conn->src->references.end(), listener) == conn->src->references.end())
		{
			conn->src->references.push_back(listener);
		}
		
		// do the same for the connection object:
		if (!conn->references->length()) conn->createPdObj(x);

		// ensure the source has this listener in it's list of references:
		if (find(conn->references.begin(), conn->references.end(), listener) == conn->references.end())
		{
			conn->references.push_back(listener);
		}
		
		// recursively refresh rest of graph:
		vAudioRenderer_refreshGraph(x, listener, conn->src);
	}
	*/
	
	/*
	// std::set method with difference computations... but need getConnectGraph
	// method to work properly!
	
	graphIterator i;

	std::set<vSoundConn*> currentConnections = getConnectedGraph(x,sinkNode);
	
	// the set of connections that used to be part of this DSP graph, but are no
	// longer connected:
	std::set<vSoundConn*> oldConnections;	
	std::set_difference(sinkNode->persistentConnections.begin(), sinkNode->persistentConnections.end(),
						currentConnections.begin(), currentConnections.end(),
						std::inserter( oldConnections, oldConnections.begin() )
						);
	
	// the set of connections that are now connected, and were not connected
	// before:
	std::set<vSoundConn*> newConnections;
	std::set_difference(currentConnections.begin(), currentConnections.end(),
						sinkNode->persistentConnections.begin(), sinkNode->persistentConnections.end(),
						std::inserter( newConnections, newConnections.begin() )
						);

	std::cout << "oldConnections:";
	for (i = oldConnections.begin(); i != oldConnections.end(); i++)
	{
		std::cout << " " << (*i)->id->s_name;
	}
	std::cout << std::endl;
	std::cout << "newConnections:";
	for (i = newConnections.begin(); i != newConnections.end(); i++)
	{
		std::cout << " " << (*i)->id->s_name;
	}
	std::cout << std::endl;
	*/
	
	
	
	/*
	for (i = newConnections.begin(); i != newConnections.end(); i++)
	{
		// create the source node (assume the sink already exists):
		if (!(*i)->src->refCount)
		{
			(*i)->src->createPdObj(x);
		}
		// increment the refCount for the source:
		(*i)->src->refCount++;
		
		// create connection pd patch if it doesn't exist:
		if (!(*i)->refCount)
		{
			(*i)->createPdObj(x);
			
			// send connection an initial 'mute' state:
			SETSYMBOL (x->coeffAtoms+0, gensym("mute"));
			SETFLOAT (x->coeffAtoms+1, (int)(*i)->mute);
			outlet_anything(x->coeff_outlet, (*i)->id, 2, x->coeffAtoms);
			
			// compute once:
			vAudioRenderer_compute(x,(*c));			
		}
		// increment the refCount for the connection:
		(*c)->refCount++;
	}

	for (i = oldConnections.begin(); i != oldConnections.end(); i++)
	{
		// decrement the reference counts on both the source and the connection,
		// and delete their pd patches if the count drops below 1:

		if (--((*i)->src->refCount) < 1)
		{
			deletePdObj((*i)->src);
			
		}
		if (--((*c)->refCount) < 1)
		{
			deletePdObj(*i);
			
		}			
		
	}
*/