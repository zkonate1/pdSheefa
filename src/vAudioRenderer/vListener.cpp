#include "vListener.h"
#include "vSoundConn.h"

#include "g_canvas.h"

using namespace std;

// *****************************************************************************
vListener::vListener(t_vAudioRenderer *x, t_symbol *id)
{

	this->node = vAudioRenderer_createSoundNode(x,id);
	
	this->node->isListener = true;

	type = gensym("listener-stereo.conn~");
	
}

vListener::~vListener()
{
	// destructor
}


void vListener::refreshConnections(t_vAudioRenderer *x)
{
	t_atom *atomList = (t_atom*) getbytes(2*sizeof(t_atom));

	connIterator c;
	for (c = node->connectFROM.begin(); c != node->connectFROM.end(); c++)
	{
		SETSYMBOL (atomList+0, gensym("setType"));
		SETSYMBOL (atomList+1, this->type);
		outlet_anything(x->coeff_outlet, (*c)->id, 2, atomList);
	}
	
}
