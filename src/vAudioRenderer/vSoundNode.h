#ifndef __vSoundNode_H
#define __vSoundNode_H

#include "vAudioRenderer.h"
#include "vRolloffTable.h"
#include "vMath.h"

class vSoundNode
{
	
public:
	
	vSoundNode(t_symbol *id);
	~vSoundNode();
	
	t_symbol *id;
	
	Vector3 pos;
	Vector3 rot;
	
	t_int active;
	t_symbol *plugin;
	vRolloffTable *rolloff;
	t_float spread; // propagation parameter for source
	t_float length; // the length of the laser and cone
	
	std::vector<vSoundConn*> connectFROM;
	
	// for dynamic patching:
	t_int obj_x;
	t_int obj_y;
	
	bool updateFlag;
	
	bool isListener;
	
	void setRolloff(t_vAudioRenderer *x, t_symbol *rolloffName);
	void createPdObj(t_vAudioRenderer *x);
	void deletePdObj(t_vAudioRenderer *x);
	
};


#endif
