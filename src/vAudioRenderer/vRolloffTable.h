

#ifndef __VROLLOFFTABLE_H
#define __VROLLOFFTABLE_H

#include "m_pd.h"

class vRolloffTable
{

	public:
		
		vRolloffTable(t_symbol *path, t_symbol *rolloffName);
		~vRolloffTable();
	
		const char *getFilename();
		bool update();
	
		t_float getValue(t_float tableIndex);

		t_symbol *path;
		t_symbol *tableName;
		t_float  *tableData;
		int       tableSize;
		
};

#endif
