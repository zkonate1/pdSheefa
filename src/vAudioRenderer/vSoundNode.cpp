#include "vSoundNode.h"
#include "vSoundConn.h"

#include "g_canvas.h"

using namespace std;


extern vAudioManager *globalAudioManager;

// *****************************************************************************
vSoundNode::vSoundNode(t_symbol *id) {
	
	this->id = id;
	this->active = 1;
	
	this->plugin = gensym("empty~");
	this->rolloff = NULL;
	this->spread = 1;
	this->length = 10;
	this->obj_x = -1;
	this->obj_y = -1;
	this->updateFlag = true;
	
	this->isListener = false;

	connectFROM.clear();
}

vSoundNode::~vSoundNode()
{
	// destructor
}


// *****************************************************************************
// We maintain a global list of rolloff tables, so each node just needs to set
// a pointer to one of these rolloff tables:
void vSoundNode::setRolloff(t_vAudioRenderer *x, t_symbol *rolloffName)
{
	if ((this->rolloff) && (rolloffName==this->rolloff->tableName)) return;

	this->rolloff = globalAudioManager->getRolloff(rolloffName);
	
	if (this->rolloff) return;
	
	// if it's not in the table, then we should create it:
	try
	{
		this->rolloff = new vRolloffTable(canvas_getdir(x->x_canvas), rolloffName);
		globalAudioManager->vRolloffList.push_back(this->rolloff); // add it to the list
		
	} catch(const char *str) {
		
		// this means that the file was not found or something... so let's just
		// set this node to "default", since we know that one works:
		post("ERROR: Node [%s] could not setRolloff to '%s'. Setting rolloff to 'default' instead.", this->id->s_name, rolloffName->s_name);
		this->setRolloff(x,gensym("default"));
	}
	
}

void vSoundNode::createPdObj(t_vAudioRenderer *x)
{
	// First check if the object already exists. This can be done by looking in
	// the <id>.patch symbol to see if the s_thing attribute is set. If so,
	// it means that the required [namecanvas] instance exists on the patch.

	t_symbol *objRecv = gensym( (char *) (string(this->id->s_name)+".patch").c_str() );
	
	if (!(objRecv->s_thing))
	{			
		// the Pd object dosn't exist, so create it:
		
		t_symbol *dynamicPatch = vAudioRenderer_getDynamicPatch(x, "SoundNodes");

		this->obj_x = DYNAMIC_CREATION_X;
		this->obj_y = DYNAMIC_CREATION_Y + (x->dynamicNodeCount * DYNAMIC_CREATION_Y_OFFSET);
		
		t_symbol *dollarZero = canvas_realizedollar(x->x_canvas, gensym("$0"));

		pdMesg(dynamicPatch, gensym("obj"), stringify((int)obj_x) + " " + stringify((int)obj_y) + " spin.SoundNode " + dollarZero->s_name + " " + id->s_name);
		x->dynamicNodeCount++;
		
		// The created object MUST have a [namecanvas] object, which creates a
		// receiver by the name: <id>.patch
		
		// We now send a loadbang message to the abstraction (since loadbangs do
		// NOT get generated for dynamic objects)
		pdMesg(objRecv, gensym("loadbang"), "");
	}
}
 
void vSoundNode::deletePdObj(t_vAudioRenderer *x)
{
	t_symbol *objRecv = gensym( (char *) (string(this->id->s_name)+".patch").c_str() );
	
	// try to ensure that object exists before we try a delete:
	if ( (obj_x>=0) && (obj_y>=0) && (objRecv->s_thing) )
	{
		t_symbol *dynamicPatch = vAudioRenderer_getDynamicPatch(x, "SoundNodes");

		DELETE_PD_OBJ_AT_COORDS(dynamicPatch, obj_x, obj_y);

	}
}
