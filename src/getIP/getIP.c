// getIP.c
//
// written by mike@mikewoz.com
//


#include <stdlib.h>
#include <arpa/inet.h>
#include <ifaddrs.h>

#include "m_pd.h"


// The Pd t_class instance, and related object struct:
static t_class *getIP_class;

typedef struct _getIP
{
	t_object x_obj;
	t_outlet *theOutlet;

} t_getIP;




static void getIP_bang(t_getIP *x)
{
	t_atom outAtoms[2];
	
	
	struct ifaddrs *interfaceArray = NULL, *tempIfAddr = NULL;
	void *tempAddrPtr = NULL;
	int rc = 0;
	char addressOutputBuffer[INET6_ADDRSTRLEN];

	char *IPaddress;
	//string IPaddress;

	
	rc = getifaddrs(&interfaceArray);  /* retrieve the current interfaces */
	if (rc == 0)
	{   
		for (tempIfAddr = interfaceArray; tempIfAddr != NULL; tempIfAddr = tempIfAddr->ifa_next)
		{
			if (tempIfAddr->ifa_addr->sa_family == AF_INET) // check if it is IP4
			{
				tempAddrPtr = &((struct sockaddr_in *)tempIfAddr->ifa_addr)->sin_addr;
				
				SETSYMBOL(outAtoms, gensym(tempIfAddr->ifa_name));

				SETSYMBOL(outAtoms+1, gensym(inet_ntop(tempIfAddr->ifa_addr->sa_family, tempAddrPtr, addressOutputBuffer, sizeof(addressOutputBuffer))));

				/*
				if (strcmp(tempIfAddr->ifa_name, "lo")!=0) // skip loopback
				{
					IPaddress = inet_ntop(tempIfAddr->ifa_addr->sa_family, tempAddrPtr, addressOutputBuffer, sizeof(addressOutputBuffer));
					
					//printf("Internet Address: [%s] %s \n", tempIfAddr->ifa_name, IPaddress.c_str());
				}
				*/			
			
				outlet_list(x->theOutlet, &s_list, 2, outAtoms);

			}
		}
	}
	
}



static void *getIP_new(void)
{
	t_getIP *x = (t_getIP *) pd_new(getIP_class);

	// create an outlet:
	x->theOutlet = outlet_new(&x->x_obj, 0);	

	return (x);
}


static void getIP_free(t_getIP *x)
{
}


void getIP_setup(void)
{
	getIP_class = class_new(gensym("getIP"), (t_newmethod)getIP_new, (t_method)getIP_free, sizeof(t_getIP), CLASS_DEFAULT, 0);
	class_addbang(getIP_class, (t_method) getIP_bang); 

	
}
