#include <stdio.h>
#include <stdlib.h>

// include necessary for PD
#include "m_pd.h"

// protocol header
#include "ubisense.h"

// The followind is support for Pd polling functions:
typedef void (*t_fdpollfn)(void *ptr, int fd);
EXTERN void sys_addpollfn(int fd, t_fdpollfn fn, void *ptr);
EXTERN void sys_rmpollfn(int fd);


#define UBISENSE_BUFFER_SIZE 256



static t_class *ubisense_class;
 
typedef struct _ubisense {

	t_object  x_obj; //obj needed by pd   	

	t_float port;
	int sock;
	
	int debugFlag;
	
	t_atom data[8];

	t_outlet *pos_outlet;
	t_outlet *button_outlet;

} t_ubisense;



t_symbol *getTagSymbol(unsigned int tagID)
{
	  char buffer [50];
	  sprintf (buffer, "%03d-%03d-%03d-%03d",
			  (tagID >> 24) & 0xFF,
			  (tagID >> 16) & 0xFF,
			  (tagID >>  8) & 0xFF,
			  (tagID >>  0) & 0xFF);
	  return gensym(buffer);
}


// =============================================================================

void ubisense_setDebug(t_ubisense *x, t_floatarg f)
{
	x->debugFlag = (int)f;
}


void ubisense_poll(t_ubisense *x, int sock)
{
	int make_sure_the_buffer_is_word_aligned ;
	char buffer[UBISENSE_BUFFER_SIZE] ;
	struct sockaddr_in host_address;
	unsigned int host_address_size = sizeof(host_address);


	int length = recvfrom(x->sock, buffer, UBISENSE_BUFFER_SIZE, 0, (struct sockaddr*) &host_address, &host_address_size);
	
	
	{
		struct LocationMessage *message = 0;
		if ((message = parse_location_message(length,buffer)))
		{
			if (x->debugFlag)
			{
				post("location: %ul in cell %ul at (%f,%f,%f) (%s) in slot %ul",
					message->tag_id_bottom,
					message->cell,
					message->x,
					message->y,
					message->z,
					(message->flags & 1 ? "valid" : "invalid"),
					message->slot);
			}
			
			SETSYMBOL(x->data+0, getTagSymbol(message->tag_id_bottom));
			SETFLOAT(x->data+1, message->x);
			SETFLOAT(x->data+2, message->y);
			SETFLOAT(x->data+3, message->z);
			outlet_list(x->pos_outlet, &s_list, 4, x->data);
			
		}
	}
	{
		struct CellEntryMessage *message = 0;
		if ((message = parse_cell_entry_message(length,buffer)))
		{
			if (x->debugFlag)
			{
				post("cell entry: %ul enters cell %ul in slot %ul",
					message->tag_id_bottom,
					message->cell,
					message->slot);
			}
		}
	}
	{
		struct CellExitMessage *message = 0;
		if ((message = parse_cell_exit_message(length,buffer)))
		{
			if (x->debugFlag)
			{
				post("cell exit: %ul leaves cell %ul in slot %ul",
					message->tag_id_bottom,
					message->cell,
					message->slot);
			}
		}
	}
	{
		struct UplinkOnlyMessage *message= 0;
		if((message = parse_uplink_only_message(length,buffer)))
		{
			if (x->debugFlag)
			{
				post("uplink: %ul uplinks in cell %ul but is not located in slot %ul with tremble switch %s",
					message->tag_id_bottom,
					message->cell,
					message->slot,
					(message->tremble_switch_on ? "on" : "off"));
			}
		}
	}
	{
		struct ButtonMessage *message= 0;
		if ((message = parse_button_message(length,buffer)))
		{
			unsigned char orange = message->orange;
			unsigned char blue = message->blue;
			if (x->debugFlag)
			{
				post("button: %ul sends %s%s%s button event in slot %ul",
					message->tag_id_bottom,
					(orange ? "orange" : ""),
					(((orange) && (blue)) ? " and " : ((orange) || (blue) ? "" : "null")),
					(blue ? "blue" : ""),
					message->slot);
			}
			
			SETSYMBOL(x->data+0, getTagSymbol(message->tag_id_bottom));
			if (orange && blue)
			{
				SETSYMBOL(x->data+1, gensym("both"));
			} else if (blue)
			{
				SETSYMBOL(x->data+1, gensym("top"));
			} else if (orange)
			{
				SETSYMBOL(x->data+1, gensym("bottom"));
			}
			outlet_list(x->button_outlet, &s_list, 2, x->data);
		}
	}


}


void *ubisense_new(t_float port)
{
	t_ubisense *x = (t_ubisense *)pd_new(ubisense_class);

	x->port = port;
	x->debugFlag = 0;
	      	   
	// create a socket to listen to:
	x->sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (x->sock < 0)
	{
		post("ERROR: could not create socket");
		return NULL;
	}
	
	struct sockaddr_in host_address;   
	memset((void*)&host_address, 0, sizeof(host_address));
	host_address.sin_family=PF_INET;
	host_address.sin_addr.s_addr=INADDR_ANY;
	host_address.sin_port=htons(x->port);
	   
	if (bind(x->sock, (struct sockaddr*)&host_address, sizeof(host_address)) < 0)
	{
		post("ERROR: could not create bind to socket");
		return NULL;
	}

	// add a callback to a polling function (will be called whenever there is
	// something to read)
	sys_addpollfn(x->sock, (t_fdpollfn)ubisense_poll, x);
	
	
	x->pos_outlet = outlet_new(&x->x_obj, &s_list);
	x->button_outlet = outlet_new(&x->x_obj, &s_list);

	return (void *)x;
}

void ubisense_free(t_ubisense *x)
{
	//sys_rmpollfn(x->sock);
}

void ubisense_setup(void) {
	
	ubisense_class = class_new(gensym("ubisense"), (t_newmethod)ubisense_new, (t_method)ubisense_free, sizeof(t_ubisense), CLASS_DEFAULT, A_DEFFLOAT, 0);

	//class_addbang   (ubisense_class, ubisense_bang);
	class_addmethod (ubisense_class, (t_method)ubisense_setDebug, gensym("setDebug"), A_DEFFLOAT, 0);
	//class_addmethod (ubisense_class, (t_method)ubisense_connect, gensym("connect"), A_DEFSYMBOL, 0);
	//class_addmethod (ubisense_class, (t_method)ubisense_refresh, gensym("refresh"), A_DEFSYMBOL, 0);
	class_sethelpsymbol(ubisense_class, gensym("help-ubisense"));
}
