#ifndef __PROTOCOL_H
#define __PROTOCOL_H

///
/// Ubisense on-the-wire protocol example
/// Author: Pete Steggles
/// 
/// Example code showing how to decode the Ubisense
/// on-the-wire protocol.
///
/// Elements currently supported:
///
/// - Cell entry event: when a tag enters a cell
/// - Cell exit event: when a tag leaves a cell
/// - Location event: when a tag is located
/// - Uplink only event: when a tag uplinks with a radio message
///                      but is not located by the cell
/// - Button event: when a tag button is pressed
///
/// Elements currently missing:
///
/// - Data delivery request: a message to the cell requesting
///                          delivery of a message to a tag in
///                          that cell
/// - Data ack event: when a data delivery request is ack'ed
///
/// Contents of this file:
///
/// - Magic numbers used to identify Ubisense packets 
/// - Structures containing Ubisense messages 
/// - Methods for parsing OTW messages into message structs 
/// - Scaffolding to create a socket, parse and print data  
///

/// Platform notes: this code is written for Unix-style networking
/// so these includes, and the socket code, will have to be changed
/// for e.g. Windows platforms (unless using cygwin or similar).

#include<string.h>
#include<sys/socket.h>
#include<netinet/in.h>


 
/// NB sizeof(int) must be 4
///    sizeof(short) must be 2
///    sizeof(float) must be 4

/// -- Magic numbers used to identify Ubisense packets --------
 
#define MAGIC_0 0xE298
#define CELL_ENTRY_MESSAGE_MAGIC_1 0x1B1
#define CELL_EXIT_MESSAGE_MAGIC_1 0x12E
#define LOCATION_MESSAGE_MAGIC_1 0x26A
#define UPLINK_ONLY_MESSAGE_MAGIC_1 0x13D
#define BUTTON_MESSAGE_MAGIC_1 0xBD

/// -- Structures containing Ubisense messages ----------------

/// The cell entry message structure 
struct CellEntryMessage {
   unsigned short magic_0 ;           
   unsigned short magic_1 ;
   unsigned int tag_id_bottom ;         // Bottom 32 bits of tag id
   unsigned int slot ;                  // Timeslot number of message
   unsigned int cell ;                  // Id of the cell that generated the message
   unsigned int dump ;                  // Filter state dump request (internal use)
} ;
 
/// The cell exit message structure 
struct CellExitMessage {
   unsigned short magic_0 ;
   unsigned short magic_1 ;
   unsigned int tag_id_bottom ;         // Bottom 32 bits of tag id
   unsigned int slot ;                  // Timeslot number of message
   unsigned int cell ;                  // Id of the cell that generated the message
} ;

/// The location message structure 
struct LocationMessage {
   unsigned short magic_0 ;
   unsigned short magic_1 ;
   unsigned int tag_id_top ;            // Top 32 bits of tag id (currently always zero)
   unsigned int tag_id_bottom ;         // Bottom 32 bits of tag id
   unsigned int flags ;                 // (flags & 1) == 'this is a valid location'
   float x ;                            // (x,y,z) position
   float y ;                            //        .
   float z ;                            //        .
   float gdop ;                         // Geometric dilution of precision 
   float error ;                        // Standard error of location calculation
   unsigned int slot ;                  // Timeslot number of message
   unsigned int slot_interval ;         // Microseconds between two timeslots (a constant
                                        // for one instance of the location system)
   unsigned int slot_delay ;            // Delay between the timeslot in which the UWB
                                        // was transmitted and the timeslot of the message
                                        // (a constant for one instance of the location system)
   unsigned int cell ;                  // Id of the cell that generated the message
} ;

/// The uplink-only message structure
struct UplinkOnlyMessage {
   unsigned short magic_0 ;
   unsigned short magic_1 ;
   unsigned int tag_id_top ;            // Top 32 bits of tag id (currently always zero)
   unsigned int tag_id_bottom ;         // Bottom 32 bits of tag id
   unsigned int slot ;                  // Timeslot number of message
   unsigned int cell ;                  // Id of the cell that generated the message
   unsigned char tremble_switch_on ;    // (tremble_switch_on & 1) == 'the tremble switch was recently activated'
   unsigned short rssi ;                // Maximum RSSI value for the tag in this cell
} ;

/// The button press message
struct ButtonMessage {
   unsigned short magic_0 ;
   unsigned short magic_1 ;
   unsigned int tag_id_bottom ;         // Bottom 32 bits of tag id
   unsigned int slot ;                  // Timeslot number of message
   unsigned char orange ;               // (orange & 1) == 'the orange button was pressed'
   unsigned char blue ;                 // (blue & 1) == 'the blue button was pressed'
} ;

/// -- Methods for parsing OTW messages into message structs --

/// Swap bytes in a 2-byte block
void swap2(char* b1, char* b2)
{
   char t = *b1;
   *b1 = *b2;
   *b2 = t;
}

/// Swap bytes in a 4-byte block
void swap4(char* b)
{
   swap2(b,b+3);
   swap2(b+1,b+2);
}

struct LocationMessage *parse_location_message (int length,char *buffer)
{
   if (length == sizeof(struct LocationMessage)) 
   {
      struct LocationMessage* message = ((struct LocationMessage *) buffer) ;
      if ((ntohs(message->magic_0) == MAGIC_0) && (ntohs(message->magic_1) == LOCATION_MESSAGE_MAGIC_1)) {
         message->tag_id_top = ntohl(message->tag_id_top) ;
         message->tag_id_bottom = ntohl(message->tag_id_bottom) ;
         message->flags = ntohl(message->flags) ;
         if (1 != ntohl(1)) {
            swap4((char*) &(message->x)) ;
            swap4((char*) &(message->y)) ;
            swap4((char*) &(message->z)) ;
            swap4((char*) &(message->gdop)) ;
            swap4((char*) &(message->error)) ;
         }
         message->slot = ntohl(message->slot) ;
         message->slot_interval = ntohl(message->slot_interval) ;
         message->slot_delay = ntohl(message->slot_delay) ;
         message->cell = ntohl(message->cell) ;
         return message ;
      }
   }
   return 0 ;
}

struct CellEntryMessage *parse_cell_entry_message (int length,char *buffer)
{
   if (length == sizeof(struct CellEntryMessage))
   {
      struct CellEntryMessage* message = ((struct CellEntryMessage *) buffer) ;
      if ((ntohs(message->magic_0) == MAGIC_0) && (ntohs(message->magic_1) == CELL_ENTRY_MESSAGE_MAGIC_1)) {
         message->tag_id_bottom = ntohl(message->tag_id_bottom) ;
         message->slot = ntohl(message->slot) ;
         message->cell = ntohl(message->cell) ;
         message->dump = ntohl(message->dump) ;
         return message ;
      }
   }
   return 0 ;
}

struct CellExitMessage *parse_cell_exit_message (int length,char *buffer)
{
   if (length == sizeof(struct CellExitMessage))
   {
      struct CellExitMessage* message = ((struct CellExitMessage *) buffer) ;
      if ((ntohs(message->magic_0) == MAGIC_0) && (ntohs(message->magic_1) == CELL_EXIT_MESSAGE_MAGIC_1)) {
         message->tag_id_bottom = ntohl(message->tag_id_bottom) ;
         message->slot = ntohl(message->slot) ;
         message->cell = ntohl(message->cell) ;
         return message ;
      }
   }
   return 0 ;
}

struct UplinkOnlyMessage *parse_uplink_only_message (int length,char *buffer)
{
   if (length == sizeof(struct UplinkOnlyMessage))
   {
      struct UplinkOnlyMessage* message = ((struct UplinkOnlyMessage *) buffer) ;
      if ((ntohs(message->magic_0) == MAGIC_0) && (ntohs(message->magic_1) == UPLINK_ONLY_MESSAGE_MAGIC_1)) {
         message->tag_id_bottom = ntohl(message->tag_id_bottom) ;
         message->slot = ntohl(message->slot) ;
         message->cell = ntohl(message->cell) ;
         message->rssi = ntohs(message->rssi) ;
         return message ;
      }
   }
   return 0 ;
}

struct ButtonMessage *parse_button_message (int length,char *buffer)
{
   if (length == sizeof(struct ButtonMessage))
   {        
      struct ButtonMessage* message = ((struct ButtonMessage *) buffer) ;
      if ((ntohs(message->magic_0) == MAGIC_0) && (ntohs(message->magic_1) == BUTTON_MESSAGE_MAGIC_1)) {
         message->tag_id_bottom = ntohl(message->tag_id_bottom) ;
         message->slot = ntohl(message->slot) ;
         return message ;
      }  
   }  
   return 0 ;
}



#endif