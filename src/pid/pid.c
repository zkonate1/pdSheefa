/* :pd: for PD - osx + linux*/
/* 2008 (C) zack settel */
// bang to get PID of this puredata process


#include <m_pd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

static t_class *pid_class;

typedef struct _pid
{
	t_object x_obj;
	t_symbol *x_id;
} t_pid;

static void pid_bang(t_pid* x)
{
	outlet_symbol(x->x_obj.ob_outlet, x->x_id);
}

static void *pid_new(void)
{
	pid_t id;
	char idstr[255];
	t_pid *x = (t_pid *)pd_new(pid_class);
	
	sprintf(idstr,"%d", (int) getpid());
	x->x_id = gensym(idstr); 
	

	outlet_new(&x->x_obj, &s_symbol);
	return (x);
}

void pid_setup(void)
{
	pid_class = class_new(gensym("pid"), (t_newmethod)pid_new, 0,sizeof(t_pid), 0,0);
	class_addbang(pid_class,pid_bang);
}
